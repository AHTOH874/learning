predicates
    inv (integer, integer)
clauses
    inv(1, 0).
    inv(0, 1).

predicates
    nor (integer, integer, integer)
clauses
    nor(0,0,1).
    nor(0,1,0).
    nor(1,0,0).
    nor(1,1,0).

predicates
    _and(integer, integer, integer)
clauses
    _and(0,0,1).
    _and(0,1,1).
    _and(1,0,1).
    _and(1,1,0).

goal
    write("Input U1 -> "), readint(U1),

    nor(X1, X2, Y1), inv(Y1, Z1),_and(X3, X4, Y2), nor(Z1, Y2, U1),
    write("Output -> "), write(X1), write(X2), write(X3), write(X4).

https://rextester.com/l/common_lisp_online_compiler


;gnu clisp  2.49.60


(defun power-help (base exponent)
  (cond ((= exponent 0) 1)
        (t (* base (power-help base (- exponent 1)))))) 


(defun task
(i x sum)
    (cond
        ((>= i 1) (task (- i 1) x (+ sum (power-help i x))))
        (T sum)
    )
)
  

(print (task 7 5 0))

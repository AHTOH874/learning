domains
    ilist = integer*

predicates
    append(ilist, ilist, ilist)
clauses
    append([], Ys, Ys).
    append([X|Xs], Ys, [X|Zs]):- append(Xs, Ys, Zs).


predicates
    counter(integer, ilist, integer, integer)
clauses
    counter(X, [], Y, Y).
    counter(X, [Head|Tail], Y, I):- Head = X, Y1 = Y + 1, counter(X, Tail, Y1, I).
    counter(X, [Head|Tail], Y, I):-  counter(X, Tail, Y, I).


predicates
    make_array(ilist)
clauses
    % make_array().
    make_array(X):- write("Input element"),
        readint(Z),!,
        append(X, [Z], O), make_array(O).
 

goal 
    make_array(L),
    write(L),nl,
    write("Input search int ->"),nl, readint(SEARCH), counter(SEARCH, L, 0, RES),
    write("Result ->"), write(RES).
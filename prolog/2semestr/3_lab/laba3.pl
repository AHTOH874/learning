songType("Ой, мороз, мороз"):-
    if("В жанре русско народной"),
    if("Про мороз").
songType("Во поле берёза стояла"):-
    if("В жанре русско народной"),
    if("Про берёзы").
    if("Часто слышимой на застольях"),
songType("50 Cent - In Da Club"):-
    if("В жанре рэп"),
    if("На английском языке").
songType("Kendrick Lamar - Backseat Freestyle"):-
    if("В жанре рэп"),
    if("На английском языке"),
    if("Популярной после 2010 года").
songType("MARIANA - создавай (Remix MODERN CLVB)"):-
    if("В жанре танцевальная"),
    if("Популярной после 2010 года"),
    if("На русском языке").

if(Characteristic):- 
    positiveAnswer(Characteristic),!.
if(Characteristic):-
    not(negativeAnswer(Characteristic)),!,
    askQuestion(Characteristic).

%rule(X) :- if(X).


startConsultation:-
    dynamic(positiveAnswer/1),
    dynamic(negativeAnswer/1),
    write("Добро пожаловать в экспертную систему подбора песни!"), nl,
    write("Ответьте на несколько вопросов, чтобы мы могли подобрать для вас подходящую песню."), nl,
    consultation.
    
consultation:-
    songType(songType),!,nl, write("Ваш выбор: "), write(songType).
consultation:-
    nl, write("Извините, мы не смогли подобрать вам песню.").

askQuestion(Description):-
    write("Песня является: "), write(Description), write(" ?"), nl,
    write("(Отвечайте да или нет):"), 
    read_string(user_input, "\n", "\r", _, Response),
    processResponse(Description, Response).

processResponse(Characteristic,"да"):- 
    asserta(positiveAnswer(Characteristic)).
processResponse(Characteristic,"нет"):- 
    asserta(negativeAnswer(Characteristic)),fail.


% edge/2 определяет ребра в графе
edge(a, b).
edge(b, c).
edge(a, d).
edge(d, e).
edge(e, f).
edge(f, g).
edge(g, h).
edge(h, i).

% path/3 определяет путь между двумя вершинами и его длину
path(X, Y, [X,Y], L) :- edge(X, Y), L is 1.
path(X, Y, [X|P], L) :- edge(X, Z), path(Z, Y, P, L1), L is L1 + 1.

% longest_path/3 находит длинный путь и определяет простой ли путь
longest_path(X, Y, Path) :-
    findall([P, L], (path(X, Y, P, L), length(P, N), N > 1), Paths),
    sort(Paths, SortedPaths),
    reverse(SortedPaths, [[Path, _]|_]).

% ?- longest_path(a, i, Path).
% Path = [a, d, e, f, g, h, i].
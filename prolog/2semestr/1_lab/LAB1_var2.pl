% set_prolog_flag(encoding, utf8).
% ['C:/learning/prolog/2semestr/1_lab/LAB1_var2.pl'].
% find_longest_path(a,i, Long), reverse(Long, A), write(A).

% edge/2 определяет ребра в графе
edge(a, b).
edge(b, c).
edge(c, d).
edge(b, d).
edge(a, d).
edge(d, e).
edge(e, f).
edge(f, i).
edge(f, g).
edge(g, h).
edge(h, i).


longest_path(Start, End, Path) :-
    dfs(Start, End, [], Path).

dfs(End, End, Visited, [End|Visited]).
dfs(Start, End, Visited, Path) :-
    edge(Start, Next),
    \+ member(Next, Visited),
    dfs(Next, End, [Start|Visited], Path).

maxlist([],[]).
maxlist([H|T], M) :- 
    maxlist(T, MT), 
    length(H, LH), 
    length(MT, LM), 
    (
        (LH > LM) -> 
            M = H ; 
            M = MT
    ).

find_longest_path(Start, End, LongPath) :-
    findall(Path, longest_path(Start, End, Path), Paths),
    maxlist(Paths,LongPath).

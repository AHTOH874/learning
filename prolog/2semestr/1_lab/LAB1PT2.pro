domains
    treetype=tree(integer, treetype, treetype); nil()

predicates
  replace(treetype, treetype, integer, integer)

clauses
  
  replace(nil, nil, _, _).
  
  replace(tree(ValueOne, tree(ValueTwo, Z, Y), tree(D, U, O)), tree(ValueTwo, tree(ValueOne, Z, Y), tree(D, U, O)), ValueOne, ValueTwo).
  replace(tree(ValueOne, tree(D, Z, Y), tree(ValueTwo, U, O)), tree(ValueTwo, tree(D, Z, Y), tree(ValueOne, U, O)), ValueOne, ValueTwo).
  replace(tree(ValueOne, tree(ValueTwo, Z, Y), tree(D, U, O)), tree(ValueTwo, tree(ValueOne, Z, Y), tree(D, U, O)), ValueTwo, ValueOne ).
  replace(tree(ValueOne, tree(D, Z, Y), tree(ValueTwo, U, O)), tree(ValueTwo, tree(D, Z, Y), tree(ValueOne, U, O)), ValueTwo, ValueOne ).


  replace(tree(P, L, R), tree(P, NewL, NewR), Left, Right):- 

    replace(L, NewL, Left, Right),
    replace(R, NewR, Left, Right).



goal
  replace(tree(6, tree(8, tree(7, nil, nil), tree(18, nil, nil)), tree(11, nil, nil)), Y, 18, 8), write(Y).

% set_prolog_flag(encoding, utf8).
% ['c:/tprolog/codes/laba2.pl'].


rule(1, "песня", "русско народная", [1]).
rule(2, "песня", "рэп", [2]).
rule(3, "песня", "танцевальная", [3]).

rule(4, "русско народная", "Ой, мороз, мороз", [1, 5]).
rule(5, "русско народная", "Во поле берёза стояла", [1]).

rule(6, "рэп", "50 Cent - In Da Club", [2, 4]).
rule(7, "рэп", "Kendrick Lamar - Backseat Freestyle", [2, 4, 6]).

rule(8, "танцевальная", "INNA - UP", [3, 4, 6]).
rule(9, "танцевальная", "MARIANA - создавай (Remix MODERN CLVB)", [3, 6]).

condition(1, "Хотите песню в жанре русская народная").
condition(2, "Хотите песню в жанре рэп").
condition(3, "Хотите песню в жанре танцевальная").
condition(4, "Песня звучит на английском языке").
condition(5, "Песня про мороз").
condition(6, "Популярна после 2010 годов").

startConsultation:-
    dynamic(positiveAnswer/1),
    dynamic(negativeAnswer/1),
    write("Добро пожаловать в экспертную систему подбора песен!"), nl,
    write("Ответьте на несколько вопросов, чтобы мы могли подобрать для вас подходящую песню."), nl,
    consultation("песня").

consultation(GameType):-
    findGame([], GameType),!.
consultation(_):- 
    nl, write("Извините, мы не смогли подобрать вам песню.").

findGame(_, GameType):-
    not(rule(_, GameType, _, _)),
    write("Результат: "), write(GameType), nl.
findGame(PassedRule, GameType):-
    rule(RuleId, GameType, GameName, Conditions),
    checkConditions(RuleId, PassedRule, Conditions),
    findGame([RuleId|PassedRule], GameName).

checkConditions(RuleId, PassedRule, [HCond|TCond]):-
    positiveAnswer(HCond),!,
    checkConditions(RuleId, PassedRule, TCond).
checkConditions(RuleId, PassedRule, [HCond|TCond]):-
    negativeAnswer(HCond),!,
    fail.
checkConditions(RuleId, PassedRule, [HCond|TCond]):-
    condition(HCond, Description),
    askQuestion(HCond, Description),
    checkConditions(RuleId, PassedRule, TCond).
checkConditions(_, _, []).

askQuestion(CondId, Question):-
    write(Question), write("? Отвечайте 1(да) или 2(нет): "),
    %read_string(user_input, "\n", "\r", _, Response),
    read(Response),
    processResponse(CondId, Response).

processResponse(CondId, 1):-
    asserta(positiveAnswer(CondId)).
processResponse(CondId, 2):-
    asserta(negativeAnswer(CondId)).


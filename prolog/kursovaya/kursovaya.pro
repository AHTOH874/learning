domains
list = symbol*

database
  answer(symbol,symbol,symbol,symbol)

predicates
  
  question(symbol,symbol)
	
  prop(symbol,symbol)  
   
  pos(symbol,symbol)    

  song_is(symbol)

  it_is(symbol)

  add_to_answer(symbol,symbol,symbol)

  auto_add_to_answer(list,symbol,symbol)

  add_to_answer_other(symbol,list)

  additional_questions(symbol,symbol)

  check(symbol,symbol,symbol)

  display_answers
  
  consultation   
   
  clear

  start
  
clauses
prop(language, "Russian").
prop(language, "English").
prop(language, "Poland").
prop(language, "Japan").

prop(style, "Rap/hip-hop").
prop(style, "Folk").
prop(style, "Club").

prop(issue, "2023").
prop(issue, "from 2000 to 2022").
prop(issue, "before 2000").

prop(class, "Lyric").
prop(class, "Joyful").
prop(class, "Another").



song_is("Oj, moroz, moroz"):-
  it_is("Russian"),
  pos(style,"Folk"),
  pos(issue,"before 2000"),
  pos(class,"Lyric").

song_is("Vo pole beryoza stoyala"):-
  it_is("Russian"),
  pos(style,"Folk"),
  pos(issue,"before 2000"),
  pos(class,"Another").

song_is("JP THE WAVY - Whats Poppin (feat. Lana)"):-
  it_is("Japan"),
  pos(style,"Rap/hip-hop"),
  pos(issue,"2023"),
  pos(class,"Another").

song_is("Skaai - SCENE!"):-
  it_is("Japan"),
  pos(style,"Rap/hip-hop"),
  pos(issue,"2023"),
  pos(class,"Lyric").

song_is("Rokiczanka - W moim ogródecku"):-
  it_is("Poland"),
  pos(style,"Folk"),
  pos(issue,"from 2000 to 2022"),
  pos(class,"Lyric").

song_is("Gdzie ta keja"):-
  it_is("Poland"),
  pos(style,"Folk"),
  pos(issue,"from 2000 to 2022"),
  pos(class,"Another").

song_is("50 Cent - In Da Club"):-
  it_is("English"),
  pos(style,"Rap/hip-hop"),
  pos(issue,"from 2000 to 2022"),
  pos(class,"Joyful").

song_is("Kendrick Lamar - Backseat Freestyle"):-
  it_is("English"),
  pos(style,"Rap/hip-hop"),
  pos(issue,"from 2000 to 2022"),
  pos(class,"Lyric").

song_is("MARIANA - создавай (Remix MODERN CLVB)"):-
  it_is("Russian"),
  pos(style,"Club"),
  pos(issue,"2023"),
  pos(class,"Another").

pos(Pre,X):- answer(Pre,X,y,_),!.
pos(Pre,X):- not(answer(Pre,X,n,_)),!, question(Pre,X).

it_is("Russian"):- pos(language,"Russian"),!.
it_is("English"):- pos(language,"English"),!.
it_is("Poland"):- pos(language,"Poland"),!.
it_is("Japan"):- pos(language,"Japan"),!.

consultation:-
  song_is(X),!,nl, write("Your choice: "), write(X), nl, clear.
consultation:-
  nl, write("Song not found"), clear.

additional_questions(language,"Country of song ").
additional_questions(style,"The style of the song is ").
additional_questions(issue,"Release date of song is "). 
additional_questions(class,"Type of song is ").

question(Pre,X):-
  additional_questions(Pre,PQ), write(PQ), write(X), write(" ?"), nl, 
  write("    Your answer (y/n/h): "), readln(Reply), add_to_answer(Pre,X,Reply).

check(Pre,Value,X):-
  prop(Pre,Value), 
  Value <> X, 
  not(answer(Pre,Value,_,_)).

auto_add_to_answer(Result,Pre,X):-
  findall(Value, check(Pre,Value,X), Result), add_to_answer_other(Pre,Result).
add_to_answer_other(Pre,[H|T]):-
  assertz(answer(Pre,H,n,auto)), add_to_answer_other(Pre,T).
add_to_answer_other(_,[]).

add_to_answer(Pre,X,y):- assertz(answer(Pre,X,y, notAuto)), auto_add_to_answer(Results,Pre,X).
add_to_answer(Pre,X,n):- assertz(answer(Pre,X,n, notAuto)),fail.
add_to_answer(Pre,X,h):- nl,write("Answers history:"),nl, display_answers, nl,nl,question(Pre,X).

display_answers:-
  answer(Pre,X,Y,AUTO), additional_questions(Pre,PQ),
  write("    - "),write(PQ),write(X), write(" - "), write(Y), write(" ("), write(AUTO), write(")"),nl, fail.
display_answers.

clear:- retract(answer(_,_,_,_)), fail.
clear.

start:- 
  consultation, nl.

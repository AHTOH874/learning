import { API_URL, get, post } from "./api"

const VEHICLE_URL = API_URL + "/vehicle"
const VEHICLE_ALL_URL = VEHICLE_URL + "/all"
const NEW_VEHICLE_URL = VEHICLE_URL + "/new"

const ANALYSIS_URL = VEHICLE_URL + "/analysis"
const MILEAGE_ANALYSIS_URL = ANALYSIS_URL + "/mileage"
const VEHICLE_ANALYSIS_URL = ANALYSIS_URL + "/vehicle"

const TYPE_URL = VEHICLE_URL + "/type"

const ALL_TYPE_URL = TYPE_URL + "/all"
const NEW_TYPE_URL = TYPE_URL + "/new"
const RENAME_TYPE_URL = TYPE_URL + "/rename"

const SCHEDULE_URL = VEHICLE_URL + "/schedule"
const TODAY_SCHEDULE_URL = SCHEDULE_URL + "/today"
const NEW_SCHEDULE_URL = SCHEDULE_URL + "/new"
const NOT_FINISHED_SCHEDULE_URL = SCHEDULE_URL + "/not_finished"

const INSPECTION_URL = VEHICLE_URL + "/inspection"
const NEW_INSPECTION_URL = INSPECTION_URL + "/new"


export async function getAllTypes() {
    return get(ALL_TYPE_URL)
}

export async function getAllVehicle() {
    return get(VEHICLE_ALL_URL)
}

export async function getSchedule() {
    return get(SCHEDULE_URL)
}

export async function getTodaySchedule() {
    return get(TODAY_SCHEDULE_URL)
}

export async function getAllInspection() {
    return get(INSPECTION_URL)
}

export async function getNotFinishedSchedule() {
    return get(NOT_FINISHED_SCHEDULE_URL)
}

export async function getVehicleAnalysis(params) {
    return post(VEHICLE_ANALYSIS_URL, params)
}

export async function getMileageAnalysis(params) {
    return post(MILEAGE_ANALYSIS_URL, params)
}


export async function newSchedule(params) {
    return post(NEW_SCHEDULE_URL, params)
}

export async function newInspection(params) {
    return post(NEW_INSPECTION_URL, params)
}

export async function newType(params) {
    return post(NEW_TYPE_URL, params)
}

export async function newVehicle(params) {
    return post(NEW_VEHICLE_URL, params)
}


export async function renameType(params) {
    return post(RENAME_TYPE_URL, params)
}
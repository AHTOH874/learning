import { API_URL, get } from "./api"

const COMMON_API_URL = API_URL + "/common";

const BRIGADE_API_URL = COMMON_API_URL + "/brigade";

export async function getBrigades() {
    return get(BRIGADE_API_URL)
}
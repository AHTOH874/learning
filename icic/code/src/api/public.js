import { API_URL, get, post } from "./api"

const PUBLIC_URL = API_URL + "/publict";

const SCHEDULE_URL = PUBLIC_URL + "/schedule"
const TODAY_SCHEDULE_URL = SCHEDULE_URL + "/today"
const NEW_SCHEDULE_URL = SCHEDULE_URL + "/new"
const FIXATE_SCHEDULE_URL = SCHEDULE_URL + "/fixate"

const ROUTE_URL = PUBLIC_URL + "/route"
const NEW_ROUTE_URL = ROUTE_URL + "/new"

const ANALYZE_URL = PUBLIC_URL + "/analyze"
const ROUTES_ANALYZE_URL = ANALYZE_URL + "/routes"
const WORKERS_ANALYZE_URL = ANALYZE_URL + "/workers"

export async function getSchedule() {
    return get(SCHEDULE_URL)
}

export async function getTodaySchedule() {
    return get(TODAY_SCHEDULE_URL)
}

export async function newSchedule(params) {
    return post(NEW_SCHEDULE_URL, params)
}

export async function fixateSchedule(params) {
    return post(FIXATE_SCHEDULE_URL, params)
}

export async function getRoutes() {
    return get(ROUTE_URL)
}

export async function newRoute(params) {
    return post(NEW_ROUTE_URL, params)
}

export async function getRoutesAnalysis(params) {
    return post(ROUTES_ANALYZE_URL, params)
}

export async function getWorkersAnalysis(params) {
    return post(WORKERS_ANALYZE_URL, params)
}
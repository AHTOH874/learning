import { API_URL, get, post } from "./api"

const CARGO_URL = API_URL + "/cargo";
const CARGO_TYPES_URL = CARGO_URL + "/types";

const SCHEDULE_URL = CARGO_URL + "/schedule"
const TODAY_SCHEDULE_URL = SCHEDULE_URL + "/today"
const NEW_SCHEDULE_URL = SCHEDULE_URL + "/new"
const FIXATE_SCHEDULE_URL = SCHEDULE_URL + "/fixate"

const ORDER_URL = CARGO_URL + "/order"
const NEW_ORDER_URL = ORDER_URL + "/new"

const ANALYZE_URL = CARGO_URL + "/analyze"
const TYPES_ANALYZE_URL = ANALYZE_URL + "/types"
const WORKERS_ANALYZE_URL = ANALYZE_URL + "/workers"

export async function getSchedule() {
    return get(SCHEDULE_URL)
}

export async function getTodaySchedule() {
    return get(TODAY_SCHEDULE_URL)
}

export async function newSchedule(params) {
    return post(NEW_SCHEDULE_URL, params)
}

export async function fixateWorkOrder(params) {
    return post(FIXATE_SCHEDULE_URL, params)
}

export async function getCargoTypes() {
    return get(CARGO_TYPES_URL)
}

export async function getOrders() {
    return get(ORDER_URL)
}

export async function newOrder(params) {
    return post(NEW_ORDER_URL, params)
}

export async function getTypesAnalysis(params) {
    return post(TYPES_ANALYZE_URL, params)
}

export async function getWorkersAnalysis(params) {
    return post(WORKERS_ANALYZE_URL, params)
}
import { API_URL, get } from "./api"

const CARS_API_URL = API_URL + "/cars";

export async function getCars() {
    return get(CARS_API_URL)
}
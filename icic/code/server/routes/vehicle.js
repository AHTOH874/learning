const { query } = require("../db");



const schedule_view = `
select distinct
    s.date, 
    brand, 
    model, 
    plate 
from inspection_schedule s
    left join vehicles v on s.id_vehicles = v.id `


module.exports = function (app) {
    app.get('/vehicle/schedule/today', (req, res) => {
        query(schedule_view + "WHERE date=NOW()::date ORDER BY date", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });

    app.get('/vehicle/schedule', (req, res) => {
        query(schedule_view + "WHERE date>=NOW()::date ORDER BY date", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });

    app.get('/vehicle/schedule/not_finished', (req, res) => {
        query(`
                select 
                    s.id as schedule_id,
                    s.date as schedule_date,
                    v.plate,
                    v.brand,
                    v.model
                from inspection_results r 
                    right join inspection_schedule s on r.id_inspection_schedule = s.id
                    left join vehicles v on s.id_vehicles = v.id 
                where r.id_inspection_schedule is null
                order by s.date DESC
            `, [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });

    app.post('/vehicle/schedule/new', (req, res) => {
        query("INSERT INTO inspection_schedule (id_vehicles, date) VALUES ($1, $2) RETURNING *", Object.values(req.body), (e, r) => {
            if (e) {
                res.send({ error: e })
                return
            }

            res.send(r.rows)
        })
    });


    app.get('/vehicle/inspection', (req, res) => {
        query(`
            select distinct 
                r.id_result,
                r.date as result_date,
                s.date as schedule_date,
                r.passed,
                r.mileage,
                r.additional_info,
                v.brand,
                v.model,
                v.plate
            from
                inspection_results r
            left join inspection_schedule s on
                s.id = r.id_inspection_schedule
            left join vehicles v on
                s.id_vehicles = v.id
        `, [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        });
    });

    app.post('/vehicle/inspection/new', (req, res) => {
        query(`
            INSERT INTO inspection_results (date, passed, mileage, additional_info, id_inspection_schedule) 
                values ($1, $2, $3, $4, $5) returning * 
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send({ error: e })
                return
            }

            res.send(r.rows)
        });
    });


    app.get('/vehicle/type/all', (req, res) => {
        query('select distinct * from vehicle_types', [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        });
    });

    app.post('/vehicle/type/new', (req, res) => {
        query('insert into vehicle_types (name) values ($1) returning *', Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        });
    });


    app.post('/vehicle/type/rename', (req, res) => {
        query('select * from rename_vehicle_types($1, $2)', Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        });
    });

    app.get('/vehicle/all', (req, res) => {
        query(`
            select distinct * from list_vehicles
        `, [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        });
    });

    app.post('/vehicle/new', (req, res) => {
        query('select * from insert_vehicles($1)', [JSON.stringify([req.body])], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        });
    })

    app.post('/vehicle/analysis/mileage', (req, res) => {
        query(`
            select
                max(mileage) - min(mileage) as total_in_org_mileage,
                plate
            from
                (
                    select
                    id_inspection_schedule,
                    mileage
                    from
                    inspection_results res
                    where res."date" >= $1 and res."date" <= $2 
                ) as r
            left join inspection_schedule s on
                s.id = r.id_inspection_schedule
            left join vehicles v on
                s.id_vehicles = v.id
            group by
                plate
            having
                count(id_vehicles) >= 2
            order  by total_in_org_mileage ASC;
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    })

    app.post('/vehicle/analysis/vehicle', (req, res) => {
        query(`
            select
                count(passed) as count_of_passed,
                v.brand
            from
                (
                    select
                        id_inspection_schedule,
                        passed
                    from
                        inspection_results res
                    where res."date" >= $1 and res."date" <= $2 
                ) as r
            left join inspection_schedule s on
                s.id = r.id_inspection_schedule
            left join vehicles v on
                s.id_vehicles = v.id
            group by
                v.brand,
                r.passed
            having
                r.passed = false
                and count(passed) > 1
            order by
                count_of_passed DESC;
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    })

}
const { query } = require("../db");

const schedule_view = `
SELECT
    o.address_from,
    o.address_to,
    c."name",
    c.phone_number,
    wo.id_brigade,
    o.date,
    v.plate
FROM
    orders o
    JOIN work_orders wo ON o.id_order = wo.id_order
    JOIN clients c ON c.id_client = o.id_client
    JOIN vehicles v ON v.id = wo.id_vehicle `

module.exports = function (app) {
    app.get('/cargo/schedule/today', (req, res) => {
        query(schedule_view + "WHERE date=NOW()::date ORDER BY date", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.get('/cargo/schedule', (req, res) => {
        query(schedule_view + "WHERE date>=NOW()::date ORDER BY date", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/cargo/schedule/fixate', (req, res) => {
        query("SELECT finish_wo($1)", Object.values(req.body), (e, r) => {
            console.log(req.body)
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/cargo/schedule/new', (req, res) => {
        query("INSERT INTO work_orders (created_at, id_order, id_brigade, id_cargo_type,id_vehicle, price, weight) VALUES (NOW(), $1, $2, $3, $4, $5, $6) RETURNING *", Object.values(req.body), (e, r) => {
            if (e) {
                res.send({ error: e })
                return
            }

            res.send(r.rows)
        })
    });
    app.get('/cargo/types', (req, res) => {
        query("SELECT * from cargo_types", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.get('/cargo/order', (req, res) => {
        query("SELECT * from orders o JOIN clients c ON c.id_client=o.id_client ORDER BY date DESC", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/cargo/order/new', (req, res) => {
        query("SELECT new_order_from_json($1)", [JSON.stringify(req.body)], (e, r) => {
            console.log(JSON.stringify(req.body))
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/cargo/analyze/types', (req, res) => {
        query(`
            SELECT
                ct."name",
                COUNT(wo.id_work_order) AS "count",
                SUM(wo.price) AS "income",
                AVG(wo.price)::INTEGER AS "avgp",
                AVG(wo.weight)::INTEGER AS "avgw"
            FROM
                cargo_types ct
                JOIN work_orders wo ON wo.id_cargo_type = ct.id_cargo_type
            WHERE wo.created_at >= $1 AND wo.created_at <= $2
            GROUP BY
                ct."name"
            ORDER BY SUM(wo.price) DESC
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/cargo/analyze/workers', (req, res) => {
        query(`
            SELECT DISTINCT
                b.id,
                STRING_AGG(
                    split_part(w.name, ' ', 1),
                    ', '
                ) as "workers",
                COUNT(*) AS "total",
                SUM(
                    CASE WHEN wo.finished_at IS NOT NULL THEN
                        1
                    ELSE
                        0
                    END
                ) AS "finished"
            FROM
                groups b
                JOIN workers w ON w.id_brigade = b.id
                JOIN work_orders wo ON wo.id_brigade = w.id_brigade
            WHERE wo.created_at >= $1 AND wo.created_at <= $2
            GROUP BY
                b.id
            ORDER BY "finished" DESC
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
};
const { query } = require("../db");

const public_view = `
SELECT DISTINCT
    s.id_schedule,
    s.schedule_date,
    r.short_name,
    r.price,
    r.start_name,
    r.stop_name,
    w.id_brigade,
    v.plate
FROM
    schedules s
    JOIN routes r ON r.id_route = s.id_route
    JOIN workers w ON w.id_brigade = s.id_brigade
    JOIN vehicles v ON v.id = s.id_vehicle
`

module.exports = function (app) {
    app.get('/publict/schedule', (req, res) => {
        query(public_view + "WHERE schedule_date >= NOW() ORDER BY schedule_date ASC LIMIT 200", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.get('/publict/schedule/today', (req, res) => {
        query(public_view + "WHERE schedule_date=NOW()::date ORDER BY schedule_date", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/publict/schedule/new', (req, res) => {
        query("INSERT INTO schedules (id_route, id_brigade, id_vehicle, schedule_date) VALUES ($1, $2, $3, $4) RETURNING *", Object.values(req.body), (e, r) => {
            if (e) {
                res.send({ error: e })
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/publict/schedule/fixate', (req, res) => {
        query("INSERT INTO reports (id_schedule, passengers_count, started_at, ended_at) VALUES ($1, $2, $3, $4) RETURNING *", Object.values(req.body), (e, r) => {
            if (e) {
                res.send({ error: e })
                return
            }

            res.send(r.rows)
        })
    });
    app.get('/publict/route', (req, res) => {
        query("SELECT DISTINCT * FROM routes", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/publict/route/new', (req, res) => {
        query("INSERT INTO routes (short_name, price, start_name, stop_name) VALUES ($1, $2, $3, $4) RETURNING *", Object.values(req.body), (e, r) => {
            if (e) {
                res.send({ error: e })
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/publict/analyze/routes', (req, res) => {
        query(`
            SELECT
                r.short_name,
                SUM(rs.passengers_count) as "passangers",
                COUNT(rs.id_report) AS "count",
                SUM(rs.passengers_count) / COUNT(rs.id_report) AS "avg",
                SUM(rs.passengers_count * r.price) AS "money"
            FROM
                routes r
                JOIN schedules s ON s.id_route = r.id_route
                JOIN reports rs ON rs.id_schedule = s.id_schedule
            WHERE s.schedule_date >= $1 AND s.schedule_date <= $2
            GROUP BY
            r.short_name
            ORDER BY SUM(rs.passengers_count * r.price) DESC
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
    app.post('/publict/analyze/workers', (req, res) => {
        query(`
            SELECT DISTINCT
                b.id,
                STRING_AGG(
                    split_part(w.name, ' ', 1),
                    ', '
                ) as "workers",
                SUM(r.passengers_count) AS "passangers",
                COUNT(r.id_report) AS "count"
            FROM
                groups b
                JOIN workers w ON w.id_brigade = b.id
                JOIN schedules s ON s.id_brigade = b.id
                JOIN reports r ON r.id_schedule = s.id_schedule
                JOIN routes rs ON rs.id_route = s.id_route
            WHERE s.schedule_date >= $1 AND s.schedule_date <= $2
            GROUP BY
                b.id
            ORDER BY COUNT(r.id_report) DESC
        `, Object.values(req.body), (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
};
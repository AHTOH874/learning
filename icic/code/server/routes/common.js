const { query } = require("../db");

module.exports = function (app) {
    app.get('/common/brigade', (req, res) => {
        query(`
            SELECT 
                b.id,
                STRING_AGG(
                    split_part(w.name, ' ', 1),
                    ', '
                ) as workers
            FROM groups b
            JOIN workers w ON w.id_brigade = b.id
            GROUP BY b.id
            `, [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
};
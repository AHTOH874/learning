const { query } = require("../db");

module.exports = function (app) {
    app.get('/cars', (req, res) => {
        query("SELECT * FROM vehicles", [], (e, r) => {
            if (e) {
                res.send(e)
                return
            }

            res.send(r.rows)
        })
    });
};
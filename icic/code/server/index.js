const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();

// parse application/json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

require("./routes/public")(app);
require("./routes/cargo")(app)
require("./routes/common")(app)
require("./routes/cars")(app)
require("./routes/vehicle")(app)

app.listen(3100, () => console.log(`Server running!`))

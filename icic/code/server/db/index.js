const { Pool } = require('pg')
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'test',
    password: 'mysecretpasswordasdfasdfasf',
    port: 5432,
})

pool.query("SELECT NOW()", (e) => {
    if (e) {
        throw e
    }
})


module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback)
    },
}
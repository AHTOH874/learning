create or replace view list_vehicles as
select
  max(ir."date") as "Последнее ТО",
  max(mileage) as "Пробег ТС",
  vt."name" as "Тип ТС",
  v2.brand,
  v2.model,
  v2.plate
from
  inspection_results ir
left join inspection_schedule is2 on
  is2.id = ir.id_inspection_schedule
left join vehicles v2 on
  is2.id_vehicles = v2.id
left join vehicle_types vt on
  v2.id_type = vt.id_type
group by
  v2.brand,
  v2.model,
  v2.plate,
  vt."name"
  



create or replace TRIGGER  insert_list_vehicles_trig
INSTEAD OF INSERT ON list_vehicles
FOR EACH row EXECUTE PROCEDURE insert_purchaseview_func();




-- drop TRIGGER insert_list_vehicles_trig on list_vehicles;
CREATE OR REPLACE FUNCTION insert_purchaseview_func()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare 
  _id_vehicles int;
  _id_type int;
  _id_inspection_schedule int;
  _id_inspection_results int;
begin
--  if not exists (select * from new where new.brand is not null and new.model is not null and new.plate is not null and new."Тип ТС" is not null and new."Пробег ТС" is not null and new."Последнее ТО" is not null) then
--    RAISE exception 'One of the values is empty';
--  end if;
  select id, id_type  into _id_vehicles, _id_type from vehicles where brand = new.brand and model = new.model and plate = new.plate;
  raise notice '_id_vehicles, _id_type: %, %', _id_vehicles, _id_type;
  select id_type into _id_type  from vehicle_types where name = new."Тип ТС";
  raise notice '_id_type: %', _id_type;
  if _id_vehicles is not null and _id_type is not null then 
--  Проверка наличия в расписании последнего то и показание пробега
  select id into _id_inspection_schedule from inspection_schedule where date = new."Последнее ТО";
  raise notice '_id_inspection_schedule: %', _id_inspection_schedule;
  if _id_inspection_schedule is not null then 
--    Проверка совпадения пробега
    select id_result into _id_inspection_results from inspection_results where mileage <= new."Пробег ТС" and date = new."Последнее ТО";
    raise notice '_id_inspection_results: %', _id_inspection_results;
    if _id_inspection_results is not null and  then
--      Добавление пробега
      update inspection_results set mileage  = new."Пробег ТС" where  id_result  = _id_inspection_results;
    end if;
    else
--      Создание ТО и прописывание туда пробега
    insert into inspection_schedule ("date", id_vehicles) values (current_date, _id_vehicles) returning id into _id_inspection_schedule;
    insert into inspection_results (id_inspection_schedule, mileage, "date") values (_id_inspection_schedule, new."Пробег ТС", new."Последнее ТО");
    end if;
  elsif _id_type is not null then
--  Создание ТС
  insert into vehicles (id_type, brand, model, plate) values (_id_type, new.brand, new.model, new.plate) returning id into _id_vehicles;
  insert into inspection_schedule ("date", id_vehicles) values (current_date, _id_vehicles) returning id into _id_inspection_schedule;
  insert into inspection_results (id_inspection_schedule, mileage, "date") values (_id_inspection_schedule, new."Пробег ТС", new."Последнее ТО");
  else 
--  создание типа, создание ТС, создание расписания ТО, создание результата ТО
  insert into vehicle_types (name) values (new."Тип ТС") returning id_type into _id_type;
  insert into vehicles (id_type, brand, model, plate) values (_id_type, new.brand, new.model, new.plate) returning id into _id_vehicles;
  insert into inspection_schedule ("date", id_vehicles) values (current_date, _id_vehicles) returning id into _id_inspection_schedule;
  insert into inspection_results (id_inspection_schedule, mileage, "date") values (_id_inspection_schedule, new."Пробег ТС", new."Последнее ТО");
  end if;

   RETURN NEW;
   	
END;
$$

insert into list_vehicles("Последнее ТО", "Пробег ТС", "Тип ТС", brand, model, plate) values (current_date+ interval '2 days', 5007, 'Легковая', 'audi', 'A7', 'К777КК58');


select * from list_vehicles lv order by "Последнее ТО" desc limit 5;




---------------------------------------------------------


CREATE OR REPLACE FUNCTION insert_new_inspection()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare 
  _id_vehicles int;
begin
  select id_vehicles into _id_vehicles  from inspection_schedule  where inspection_schedule.id = new.id_inspection_schedule;
  IF new.passed THEN
      insert into public.inspection_schedule("date", id_vehicles ) values(new.date + interval '1 years', _id_vehicles);
  elsif not new.passed then
      insert into public.inspection_schedule("date", id_vehicles ) values(new.date +  interval '1 days', _id_vehicles);
  END IF;
  RETURN NEW;
END;
$$


create or replace trigger after_insert_result after insert on inspection_results for each row execute procedure insert_new_inspection();


select * from inspection_results ir order by id_result desc limit 10;

select * from inspection_schedule is2 where id ;

select * from inspection_schedule is2 order by id desc limit 10;

insert into public.inspection_schedule("date", id_vehicles) values(now(), 85)

insert into inspection_results("date", passed, mileage, id_inspection_schedule) values(now(), true, 100000, 1004)

---------------------------------


select * from list_vehicles limit 10;

create view list_vehicles as
select
  max(ir."date") as "Последнее ТО",
  max(mileage) as "Пробег ТС",
  vt."name" as "Тип ТС",
  v2.brand,
  v2.model,
  v2.plate
from
  inspection_results ir
left join inspection_schedule is2 on
  is2.id = ir.id_inspection_schedule
left join vehicles v2 on
  is2.id_vehicles = v2.id
left join vehicle_types vt on
  v2.id_type = vt.id_type
group by
  is2.id_vehicles,
  v2.brand,
  v2.model,
  v2.plate,
  vt."name"



CREATE TRIGGER insert_list_vehicles_trig
INSTEAD OF INSERT ON list_vehicles
FOR EACH ROW EXECUTE PROCEDURE inser_list_vehicles_func();

CREATE OR REPLACE FUNCTION insert_purchaseview_func()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare 
  _id_vehicles int;
  _id_type int;
  _id_inspection_schedule int;
  _id_inspection_results int;
begin
  if exists( select id into _id_vehicles, id_type into _id_type from vehicles where brand = new.brand and model = new.model and plate = new.plate) then 
--  Проверка наличия в расписании последнего то и показание пробега
    if exists ( select id into _id_inspection_schedule from inspection_schedule where date = new."Последние ТО") then 
--    Проверка совпадения пробега
    else
    	insert into inspection_schedule("date", id_vehicles) values (new."Последние ТО", _id_vehicles) returning id into _id_inspection_schedule;
    	insert into inspection_results()
--    Создание ТО и прописывание туда пробега
    end if;
  elsif exists (select id_type into _id_type from vehicles_types where name = new."Тип ТС") then
--  Создание ТС
  else 
--  создание типа, создание ТС, создание расписания ТО, создание результата ТО
  end if;
  
  
   INSERT INTO purchase(product_id, when_bought)
   SELECT (SELECT product_id FROM product WHERE product_name = NEW.product_name), NEW.when_bought
   RETURNING purchase_id
   INTO   NEW.purchase_id;  -- generated serial ID for RETURNING - if needed

   RETURN NEW;
END;
$$
----------------------------


-- Список машин не прошедших ТО больше 1 раз
select
    count(passed),
    v2.brand
from
    (
        select
            id_inspection_schedule,
            passed
        from
            inspection_results res
        where res."date" >= $1 and res."date" <= $2 
    ) as r
left join inspection_schedule s on
    s.id = r.id_inspection_schedule
left join vehicles v on
    s.id_vehicles = v.id
group by
    v.brand,
    r.passed
having
    r.passed = false
    and count(passed) > 1
order by
    v.brand asc;


--Кол-во пробега в милях за время работы в орг-ии
select
    max(mileage) - min(mileage) as total_in_org_mileage,
    plate
from
    (
        select
          id_inspection_schedule,
          mileage
        from
          inspection_results res
        where res."date" >= $1 and res."date" <= $2 
    ) as r
left join inspection_schedule s on
  s.id = ir.id_inspection_schedule
left join vehicles v on
  s.id_vehicles = v.id
group by
  plate
having
  count(id_vehicles) > 2;

drop view if exists list_vehicles;

create view list_vehicles as
select
  max(ir."date") as "Последнее ТО",
  max(mileage) as "Пробег ТС",
  vt."name" as "Тип ТС",
  v2.brand,
  v2.model,
  v2.plate
from
  inspection_results ir
left join inspection_schedule is2 on
  is2.id = ir.id_inspection_schedule
left join vehicles v2 on
  is2.id_vehicles = v2.id
left join vehicle_types vt on
  v2.id_type = vt.id_type
group by
  is2.id_vehicles,
  v2.brand,
  v2.model,
  v2.plate,
  vt."name"

select
  *
from
  list_vehicles;

create or replace
function insert_vehicles(in data json) returns integer[]  as $$ 
declare
  js json := data;
  i json;
  return_id int;
  results_ids integer[];
begin
  if json_array_length(data) = 0 then raise exception 'Invalid array for insert function'; end if;
  for i in select  * from json_array_elements(js) loop raise notice 'Json element: %',  i;
    if i->'model' is null then raise exception 'Element model is null in %',i; end if;
    if i->'brand' is null then raise exception 'Element brand is null in %',i; end if;
    if i->'plate' is null then raise exception 'Element plate is null in %',i; end if;
    if i->'id_type' is null then raise exception 'Element id_type is null in %',i;
    else
      if not exists(select id_type, name from vehicle_types vt where vt.id_type = cast(i->>'id_type' as integer))
      then
      	if i->'type_name' is null then
      		raise exception 'Element type_name required when id_type does not exists';
      	else
        	insert into vehicle_types OVERRIDING SYSTEM VALUE values (cast(i->>'id_type' as integer), i->>'type_name');
        end if;
      end if;
    end if;
    insert into vehicles(brand, model, plate, id_type) values 
      (i->>'brand', i->>'model', i->>'plate', cast(i->>'id_type' as integer)) RETURNING id into return_id;
    results_ids := array_append(results_ids, return_id);
  end loop;
  return results_ids;
end $$ language plpgsql;

DROP FUNCTION rename_vehicle_types(integer,character varying);

create or replace function rename_vehicle_types(in type_id integer, in new_name varchar) returns setof vehicle_types as $$
begin 
  if exists(select * from vehicle_types vt where id_type = type_id) then 
    return query update vehicle_types set "name" = new_name where id_type = type_id returning vehicle_types.*;
    
  else
    raise exception 'Vehicle type with id % not found', type_id;
  end if;
end
$$ language  plpgsql;

select * from rename_vehicle_types(31, 'Полуприцеп')


-- При update viev раскидываем на несколько таблиц
-- 2. Триггер после результат ТО, то добавляем новую дату осмотра (Если не успешно, то через 1 день то, если успешно то через пол года)

CREATE OR REPLACE FUNCTION insert_new_inspection()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	IF NEW.last_name <> OLD.last_name THEN
		 insert into public.inspection_schedule()
		 values()
	END IF;

	RETURN NEW;
END;
$$


create trigger after_success_to after insert on inspection_results where passed = true 


select
  *
from
  insert_vehicles('[
        {
            "model": "1",
            "brand": "1",
            "plate": "1",
            "id_type": 31,
      "type_name": "Прицеп"
        },
    {
            "model": "2",
            "brand": "2",
            "plate": "2",
            "id_type": 32,
      "type_name": "Прицеп"
        }
    ]
    ');
 
   
   
 
 select * from  vehicles v2 where v2.id = 207
import sqlite3 as sl

import random
import os

os.remove('lab4.db')

con = sl.connect('lab4.db')

# Создание таблицы Должности
with con:
    con.execute("""
        create table Post (
            ID integer NOT NULL primary key AUTOINCREMENT,
            Name text NOT NULL
        )    
    """)

# Создание таблицы Кафедры
with con:
    con.execute("""
        create table Department (
            ID integer NOT NULL primary key AUTOINCREMENT,
            Name text NOT NULL,
            Institute text NOT NULL
        )   
    """)

# Создание таблицы Преподаватели
with con:
    con.execute("""
        create table Teacher (
            ID integer NOT NULL primary key AUTOINCREMENT,
            FIO text NOT NULL,
            Age integer NOT NULL,
            ID_Department integer,
            ID_Post integer,
            FOREIGN KEY (ID_Department) REFERENCES Department(ID),
            FOREIGN KEY (ID_Post) REFERENCES Post(ID)
        )
    """)


posts = ["Доцент", "Заведующий лабораторией", "Инженер", "Старший преподаватель", "Заведующий лабораторией",  "Ведущий программист"]

# Заполнение таблицы Должности
for post in posts:
    con.execute(f'insert into Post(Name) values("{post}")')

# data = con.execute("SELECT * FROM Post")
# for row in data:
#     print(row)


Departments = ["Вычислительная техника", "Высшая и прикладная математика", "Информационно-вычислительные системы", "Математичесое обеспечение и применение ЭВМ"]

# Заполнение таблицы Кафедры
for department in Departments:
    con.execute(f'insert into Department(Name, Institute) values("{department}", "ПГУ")')

# data = con.execute("SELECT * FROM Department")
# for row in data:
#     print(row)


# Заполненние таблицы Преподаватели
for index in range(20):
    post_id = random.randint(1,len(posts))
    dep_id = random.randint(1,len(Departments))
    age = random.randint(20, 70)

    con.execute(f'insert into Teacher(FIO, Age, ID_Department, ID_Post) values ("Фамилия Имя Отчество{index}", {age}, {dep_id}, {post_id})')

# data = con.execute("SELECT * FROM Teacher")
# for row in data:
#     print(row)

# Выведите кафедру, на которой работает больше всего сотрудников
print('Выведите кафедру, на которой работает больше всего сотрудников')
data = con.execute("""
    select Department.Name, count(ID_Department) as ccount from Teacher left join Department on ID_Department = Department.ID group by ID_Department order by ccount desc limit 1
""")
for row in data:
    print(row[0])

# Cписок кафедр в порядке убывания количества сотрудников
print('\nCписок кафедр в порядке убывания количества сотрудников')
data = con.execute("""
    select Department.Name, count(ID_Department) as ccount from Teacher left join Department on ID_Department = Department.ID group by ID_Department order by ccount desc
""")
for row in data:
    print(f'В кафедре {row[0]} {row[1]} сотрудников')


# выведите «самую молодую» кафедру (возраст кафедры = сложить возраст сотрудников и поделить на их количество).
print('\n"Самая молодая" кафедра  (возраст кафедры = сложить возраст сотрудников и поделить на их количество)')

data = con.execute("""
    select Department.Name, SUM(age)/count(ID_Department) as avg_age from Teacher left join Department on ID_Department = Department.ID group by ID_Department order by avg_age asc limit 1
""")
for row in data:
    print(row[0])



import sqlite3 as sl
import os
import csv

os.remove('my-test.db')

con = sl.connect('my-test.db')

# Создание таблицы Город
with con:
    con.execute("""
        create table City (
            ID integer NOT NULL primary key AUTOINCREMENT,
            Name text NOT NULL
        )
    """)


# Создание таблицы Страна
with con:
    con.execute("""
        create table Side (
            ID integer NOT NULL primary key AUTOINCREMENT,
            Name text NOT NULL,
            ID_City integer,
            FOREIGN KEY (ID_City) REFERENCES City(ID)
        )        
    """)
# Создание таблицы Улица
with con:
    con.execute("""
        create table Street (
            ID integer NOT NULL primary key AUTOINCREMENT,
            Name text NOT NULL,
            ID_City integer,
            FOREIGN KEY (ID_City) REFERENCES City(ID)
        );
    """)



# Функция вывода всех Городов
def print_City():
    with con:
        print('Города:')
        print(f'{"ID":2} | {"Name":25}')
        data = con.execute("SELECT * FROM City")
        for row in data:
            print(f'{row[0]:2} | {row[1]:25}')
    print()


# Функция вывода всех Улиц
def print_Street():
    with con:
        print('Улицы:')
        print(f'{"ID":2} | {"Name":25} | {"ID_City":3}')
        data = con.execute("SELECT * FROM Street")
        for row in data:
            print(f'{row[0]:2} | {row[1]:25} | {row[2]:25}')
    print()


# Функция вывода всех Стран
def print_Side():
    with con:
        print('Страны:')
        print(f'{"ID":2} | {"Name":25} | {"ID_City":3}')
        data = con.execute("SELECT * FROM Side")
        for row in data:
            print(f'{row[0]:2} | {row[1]:25} | {row[2]:25}')
    print()


# Функция добавления записи о Городе
def insert_city(Name):
    con.execute(f'INSERT INTO City(Name) VALUES("{Name}")')


# Функция добавления записи о Улице
def insert_street(Name, ID_City):
    con.execute(f'INSERT INTO Street(Name, ID_City) VALUES("{Name}", {ID_City})')


# Функция добавления записи о Стране
def insert_side(Name, ID_City):
    con.execute(f'INSERT INTO Side(Name, ID_City) VALUES("{Name}", {ID_City})')


def insert(table):
    if table == "City":
        name = input("Название города: ")
        insert_city(name)
    if table == "Street":
        name = input("Название улицы: ")
        ID_City = input("ID города: ")
        insert_street(name, ID_City)
    if table == "Side":
        name = input("Название страны: ")
        ID_City = input("ID города: ")
        insert_rebenok(name, ID_City)


def view(table):
    if table == "City":
        print_City()
    if table == "Street":
        print_Street()
    if table == "Side":
        print_Side()


def delete(table, id):
    with con:
        con.execute(f'DELETE FROM {table} where ID = {id}')


def change(table, column, id, value):
    with con:
        con.execute(f'UPDATE {table} SET {column} = "{value}" where ID = {id}')


def export_table(table):
    file = open(f'{table}.csv', 'w', newline='')
    with file:
        writer = csv.writer(file)

        data = con.execute(f'SELECT * FROM {table}')
        names = [description[0] for description in data.description]
        writer.writerow(names)
        for row in data:
            writer.writerow(row)
    print(f'Таблица экспортирована в {table}.csv\n')


def import_csv(table):
    data = con.execute(f'SELECT * FROM {table}')

    names = [description[0] for description in data.description]
    names.remove("ID")

    imported = input("Напишите название файла. Пример: data.csv\n")

    locked = False

    if os.path.exists(imported):
        with open(imported, 'r') as file:
            dr = csv.DictReader(file)
            if names != [[j for j in i if j in names] for i in dr][0]:
                locked = True
                print("Поля файла не соответствуют полям выбранной таблицы!\n")
        file.close()
        if not locked:
            with open(imported, 'r') as file:
                dr = csv.DictReader(file)
                to_db = [[i[j] for j in i if j in names] for i in dr]
                for row in to_db:
                    row = ["'" + i + "'" for i in row]
                    con.execute("INSERT INTO {0}({1}) VALUES({2})".format(table, ','.join(names), ','.join(row)))
                view(table)
            file.close()
        else:
            print("Файл не существует")


insert_city('Пенза')
insert_side('Россия', 1)
insert_street('Московская', 1)

tables = ["City", "Street", "Side"]
while 1:
    print("1. Города")
    print("2. Улицы")
    print("3. Страны")
    print("0. Закончить работу")
    
    selected_table = int(input("Выберите таблицу или действие: "))
    if selected_table == 0:
        break
    table = tables[selected_table - 1]
    view(table)

    while 1:
        print("1. Посмотреть таблицу")
        print("2. Добавить запись")
        print("3. Изменить запись")
        print("4. Удалить запись")
        if table == 'Side':
            print("5. Посмотреть информацию о странах")
        if table == 'City':
            print("5. Посмотреть информацию о городе и количестве улиц в нём")
        print("8. Импорт таблицы из CSV")
        print("9. Экспорт таблицы в CSV")
        print("0. Закончить с этой таблицей")

        action = int(input("Выберите действие: "))

        if action == 1:
            view(table)
        if action == 2:
            insert(table)
        if action == 3:
            column = input("Поле: ")
            id = input("ID изменяемой строки: ")
            value = input("Новое значение: ")
            change(table, column, id, value)
            print("Запись измненена\n")
        if action == 4:
            id = input("ID удаляемой строки: ")
            delete(table, id)
            print("Запись удалена\n")
        if action == 5:
            if table == 'Side':
                print(f'{"Название страны":25} | {"Название города":25}')
                data = con.execute("select s.name, c.name  from Side s join City c on s.ID_City = c.ID")
                for row in data:
                    print(f'{row[0]:25} | {row[1]:25}')

            if table == 'City':
                print(f'{"Название города":25} | {"Количество улиц":11}')
                data = con.execute("select c.name, count(s.name) from City c left join Street s on c.ID = s.ID_City group by c.name")
                for row in data:
                    print(f'{row[0]:25} | {row[1]:11}')
            print()
        if action == 8:
            import_csv(table)
        if action == 9:
            export_table(table)
        if action == 0:
            break

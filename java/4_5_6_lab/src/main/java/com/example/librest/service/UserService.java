package com.example.librest.service;

import com.example.librest.model.UserEntity;
import com.example.librest.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserEntity createUser(String username, String password, String role) {
        return userRepository.save(new UserEntity(username, password, role));
    }

    public UserEntity findUserById(long id) {
        return userRepository.findById(id).orElse(null);
    }

    public UserEntity findUserByName(String username) {
        return userRepository.findByName(username);
    }

    public UserEntity updateUser(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

}

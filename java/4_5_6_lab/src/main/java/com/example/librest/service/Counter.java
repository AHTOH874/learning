package com.example.librest.service;

import org.bson.Document;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class Counter {

    private final JdbcTemplate jdbcTemplate;

    public Counter(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void increment() {
        int count = get();
        if(count == 0) {
            jdbcTemplate.update("INSERT INTO counter VALUES (?)", 1);
        } else {
            jdbcTemplate.update("UPDATE counter SET count = ?", count + 1);
        }
    }

    public int get() {
        return jdbcTemplate.query("SELECT count FROM counter", rs -> {
            if(rs.next()) {
                return rs.getInt("count");
            }
            return 0;
        });
    }

}

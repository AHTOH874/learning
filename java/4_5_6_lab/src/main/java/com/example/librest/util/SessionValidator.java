package com.example.librest.util;

import com.example.librest.model.UserEntity;
import com.example.librest.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.function.Supplier;

@Component
public class SessionValidator {

    private final UserService userService;

    public SessionValidator(UserService userService) {
        this.userService = userService;
    }

    public ResponseEntity<Object> validate(HttpSession session, Function<UserEntity, ResponseEntity<Object>> resultFunction) {
        Long currentUserId = (Long) session.getAttribute("userId");
        if(currentUserId == null) {
            return ResponseEntity.status(403).build();
        }

        UserEntity user = userService.findUserById(currentUserId);
        if(user == null) {
            return ResponseEntity.status(403).build();
        }
        try {
            return resultFunction.apply(user);
        } catch (Throwable throwable) {
            return ResponseEntity.status(400).body(throwable.getMessage());
        }
    }

}

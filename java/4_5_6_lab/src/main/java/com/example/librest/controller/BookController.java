package com.example.librest.controller;

import com.example.librest.model.Book;
import com.example.librest.service.BookService;
import com.example.librest.service.UserService;
import com.example.librest.util.SessionValidator;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;
    private final SessionValidator sessionValidator;
    private final UserService userService;

    public BookController(BookService bookService, SessionValidator sessionValidator, UserService userService) {
        this.bookService = bookService;
        this.sessionValidator = sessionValidator;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<Object> allBooks(HttpSession httpSession) {
        return sessionValidator.validate(httpSession, (user) -> ResponseEntity.ok(bookService.findAll()));
    }

    @PostMapping
    public ResponseEntity<Object> addBook(@RequestBody Book book, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, (user) -> {
            book.setId(UUID.randomUUID().toString());
            bookService.addBook(book);
            return ResponseEntity.ok(bookService.findAll());
        });
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findBook(@PathVariable String id, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(!user.getRole().equals("admin") && !user.getRole().equals("moderator")) {
                return ResponseEntity.status(403).build();
            }
            Book book = bookService.searchById(id);
            if(book == null) {
                return ResponseEntity.status(404).build();
            }
            return ResponseEntity.ok(book);
        });
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> updateBook(@PathVariable String id, @RequestBody Book newBook, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(!user.getRole().equals("admin") && !user.getRole().equals("moderator")) {
                return ResponseEntity.status(403).build();
            }
            Book book = bookService.searchById(id);
            if(book == null) {
                return ResponseEntity.status(404).build();
            }
            if(newBook.getName() != null)
                book.setName(newBook.getName());
            if(newBook.getAuthor() != null)
                book.setAuthor(newBook.getAuthor());
            if(newBook.getUrl() != null)
                book.setUrl(newBook.getUrl());
            bookService.updateBook(book);
            return ResponseEntity.ok(book);
        });
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteBook(@PathVariable String id, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(!user.getRole().equals("admin") && !user.getRole().equals("moderator")) {
                return ResponseEntity.status(403).build();
            }
            bookService.removeBook(id);
            return ResponseEntity.ok(bookService.findAll());
        });
    }

}

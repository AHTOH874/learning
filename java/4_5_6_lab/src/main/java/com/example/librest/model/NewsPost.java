package com.example.librest.model;

import jakarta.persistence.*;

@Entity
@Table(name = "lib_news")
public class NewsPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String header;
    private String text;

    public NewsPost(String header, String text) {
        this.header = header;
        this.text = text;
    }

    public NewsPost() {}

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

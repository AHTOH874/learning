package com.example.librest.controller;

import com.example.librest.service.Counter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/info")
public class InfoController {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private final Counter counter;

    public InfoController(Counter counter) {
        this.counter = counter;
    }

    @GetMapping
    public ResponseEntity<Object> getInfo() {
        return ResponseEntity.ok(
                Map.of("counter", counter.get(), "date", SIMPLE_DATE_FORMAT.format(new Date(System.currentTimeMillis())))
        );
    }

}

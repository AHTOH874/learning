package com.example.librest.model;

import org.bson.Document;

public class Book {

    private String id;
    private String name;
    private String author;
    private String url;

    public Book(String id, String name, String author, String url) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.url = url;
    }

    public static Book fromDocument(Document document) {
        return new Book(
                document.get("_id", String.class),
                document.get("name", String.class),
                document.get("author", String.class),
                document.get("url", String.class)
        );
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getUrl() {
        return url;
    }

    public Document toDocument() {
        return new Document("_id", id)
                .append("name", name)
                .append("author", author)
                .append("url", url);
    }

}

package com.example.librest.controller;

import com.example.librest.model.NewsPost;
import com.example.librest.repository.NewsRepository;
import com.example.librest.util.SessionValidator;
import jakarta.servlet.http.HttpSession;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/news")
public class NewsController {

    private final NewsRepository newsRepository;
    private final SessionValidator sessionValidator;

    public NewsController(NewsRepository newsRepository, SessionValidator sessionValidator) {
        this.newsRepository = newsRepository;
        this.sessionValidator = sessionValidator;
    }

    @GetMapping
    public ResponseEntity<Object> findAll(HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> ResponseEntity.ok(newsRepository.findAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findNews(@PathVariable String id, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> ResponseEntity.ok(newsRepository.findById(Long.parseLong(id))));
    }

    @PostMapping
    public ResponseEntity<Object> createNews(@RequestBody NewsPost newsPost, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(user.getRole().equals("user")) {
                return ResponseEntity.status(403).build();
            }
            newsRepository.save(newsPost);
            return ResponseEntity.ok(newsRepository.findAll());
        });
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> editNews(@PathVariable String id, @RequestBody NewsPost newsPost, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(user.getRole().equals("user")) {
                return ResponseEntity.status(403).build();
            }
            Optional<NewsPost> newsPostOptional = newsRepository.findById(Long.parseLong(id));
            if(newsPostOptional.isPresent()) {
                NewsPost post = newsPostOptional.get();
                if(newsPost.getHeader() != null)
                    post.setHeader(newsPost.getHeader());
                if(newsPost.getText() != null)
                    post.setText(newsPost.getText());
                newsRepository.save(post);
            } else {
                return ResponseEntity.status(404).build();
            }
            return ResponseEntity.ok(newsRepository.findAll());
        });
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteNews(@PathVariable String id, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(user.getRole().equals("user")) {
                return ResponseEntity.status(403).build();
            }
            newsRepository.deleteById(Long.parseLong(id));
            return ResponseEntity.ok(newsRepository.findAll());
        });
    }

}

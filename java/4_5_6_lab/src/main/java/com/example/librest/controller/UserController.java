package com.example.librest.controller;

import com.example.librest.model.UserEntity;
import com.example.librest.service.UserService;
import com.example.librest.util.SessionValidator;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final SessionValidator sessionValidator;

    public UserController(UserService userService, SessionValidator sessionValidator) {
        this.userService = userService;
        this.sessionValidator = sessionValidator;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> getUser(@PathVariable String id) {
        try {
            return ResponseEntity.ok(userService.findUserById(Long.parseLong(id)));
        } catch (NumberFormatException ex) {
            return ResponseEntity.status(400).build();
        }
    }

    @GetMapping("/search")
    public ResponseEntity<Object> searchUser(@RequestParam String username) {
        return ResponseEntity.ok(userService.findUserByName(username));
    }

    @GetMapping
    public ResponseEntity<Object> findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable String id, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(!user.getRole().equals("admin")) {
                return ResponseEntity.status(403).build();
            }
            userService.deleteUser(Long.parseLong(id));
            return ResponseEntity.ok(Map.of("deleted", id));
        });
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> editUser(@PathVariable String id, @RequestBody UserEntity entity, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            if(!user.getRole().equals("admin")) {
                return ResponseEntity.status(403).build();
            }
            UserEntity target = userService.findUserById(Long.parseLong(id));
            if(target == null) {
                return ResponseEntity.status(404).build();
            }

            if (entity.getUsername() != null)
                target.setUsername(entity.getUsername());
            if (entity.getPassword() != null)
                target.setPassword(entity.getPassword());
            if (entity.getRole() != null)
                target.setRole(entity.getRole());

            userService.updateUser(target);
            return ResponseEntity.ok(userService.findAll());
        });
    }

}

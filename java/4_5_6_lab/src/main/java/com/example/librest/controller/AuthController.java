package com.example.librest.controller;

import com.example.librest.model.AuthCredentials;
import com.example.librest.model.UserEntity;
import com.example.librest.service.Counter;
import com.example.librest.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AuthController {

    private final UserService userService;
    private final Counter counter;

    public AuthController(UserService userService, Counter counter) {
        this.userService = userService;
        this.counter = counter;
    }

    @PostMapping("/register")
    public ResponseEntity<Object> registerUser(@RequestBody AuthCredentials authCredentials, HttpSession httpSession) {

        if(authCredentials.getUsername() == null || authCredentials.getPassword() == null) {
            return ResponseEntity.status(400).body("username_or_password_is_undefined");
        }

        UserEntity user = userService.findUserByName(authCredentials.getUsername());
        if(user != null) {
            return ResponseEntity.status(409).body("account_already_exists");
        }

        user = userService.createUser(authCredentials.getUsername(), authCredentials.getPassword(), "user");
        httpSession.setAttribute("userId", user.getId());
        counter.increment();

        return ResponseEntity.ok(user);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> loginUser(@RequestBody AuthCredentials authCredentials, HttpSession httpSession) {

        if(authCredentials.getUsername() == null || authCredentials.getPassword() == null) {
            return ResponseEntity.status(400).body("username_or_password_is_undefined");
        }

        UserEntity user = userService.findUserByName(authCredentials.getUsername());
        if(user == null) {
            return ResponseEntity.status(404).body("account_not_found");
        }
        if(!user.getPassword().equals(authCredentials.getPassword())) {
            return ResponseEntity.status(400).body("wrong_password");
        }

        httpSession.setAttribute("userId", user.getId());
        counter.increment();

        return ResponseEntity.ok(user);
    }

    @PostMapping("/logout")
    public ResponseEntity<Object> logoutUser(HttpSession httpSession) {

        Long currentUserId = (Long) httpSession.getAttribute("userId");
        if(currentUserId == null) {
            return ResponseEntity.status(401).body("you_are_not_authorized");
        }

        httpSession.invalidate();
        return ResponseEntity.ok("successfully_logout");
    }

    @GetMapping
    public ResponseEntity<Object> currentAccount(HttpSession httpSession) {
        Long currentUserId = (Long) httpSession.getAttribute("userId");
        if(currentUserId == null) {
            return ResponseEntity.status(401).body("you_are_not_authorized");
        }

        UserEntity user = userService.findUserById(currentUserId);
        if(user == null) {
            return ResponseEntity.status(401).body("you_are_not_authorized");
        }

        return ResponseEntity.ok(user);
    }

}

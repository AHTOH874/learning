package com.example.librest.controller;

import com.example.librest.util.SessionValidator;
import jakarta.servlet.http.HttpSession;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/files")
public class FileController {

    private final SessionValidator sessionValidator;

    public FileController(SessionValidator sessionValidator) {
        this.sessionValidator = sessionValidator;
    }

    @GetMapping(value = "/{name}", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<Resource> getFile(@PathVariable String name) {
        FileSystemResource fileSystemResource = new FileSystemResource("C:/learning/java/5_lab/images/" + name);
        if(fileSystemResource.exists())
            return ResponseEntity.ok(fileSystemResource);
        return ResponseEntity.status(404).build();
    }

    @PostMapping(value = "/image")
    public ResponseEntity<Object> uploadFile(HttpSession httpSession, @RequestBody MultipartFile file) {
        return sessionValidator.validate(httpSession, user -> {
            try {
                file.transferTo(new File("C:/learning/java/5_lab/images/" + user.getId() + ".jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ResponseEntity.ok(user.getId());
        });
    }

    @GetMapping(value = "/image/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Object> getImage(@PathVariable String id, HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            FileSystemResource fileSystemResource = new FileSystemResource("C:/learning/java/5_lab/images/" + id + ".jpg");
            if(fileSystemResource.exists())
                return ResponseEntity.ok(fileSystemResource);
            return ResponseEntity.status(404).build();
        });
    }

    @DeleteMapping("/image")
    public ResponseEntity<Object> deleteImage(HttpSession httpSession) {
        return sessionValidator.validate(httpSession, user -> {
            File file = new File("C:/learning/java/5_lab/images/" + user.getId() + ".jpg");
            if(file.exists())
                file.delete();
            return ResponseEntity.ok(user.getId());
        });
    }

}

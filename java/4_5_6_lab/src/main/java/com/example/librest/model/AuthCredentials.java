package com.example.librest.model;

public class AuthCredentials {

    private String username;
    private String password;

    public AuthCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public AuthCredentials() {}

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}

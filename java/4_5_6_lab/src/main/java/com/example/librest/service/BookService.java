package com.example.librest.service;

import com.example.librest.model.Book;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {

    private final MongoCollection<Document> bookCollection;

    public BookService(MongoClient mongoClient) {
        bookCollection = mongoClient.getDatabase("local").getCollection("books");
    }

    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();
        bookCollection.find().forEach(document -> {
            books.add(Book.fromDocument(document));
        });
        return books;
    }

    public Book searchByName(String bookName) {
        Document document = bookCollection.find(new Document("name", bookName)).first();
        if(document == null)
            return null;
        return Book.fromDocument(document);
    }

    public Book searchById(String id) {
        Document document = bookCollection.find(new Document("_id", id)).first();
        if(document == null)
            return null;
        return Book.fromDocument(document);
    }

    public void addBook(Book book) {
        bookCollection.insertOne(book.toDocument());
    }

    public void updateBook(Book book) {
        bookCollection.updateOne(new Document("_id", book.getId()), Updates.combine(
                Updates.set("name", book.getName()),
                Updates.set("author", book.getAuthor()),
                Updates.set("url", book.getUrl())
        ));
    }

    public void removeBook(String id) {
        bookCollection.deleteOne(new Document("_id", id));
    }

}

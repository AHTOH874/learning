package com.example.librest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class LibRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(LibRestApplication.class, args);
    }

}

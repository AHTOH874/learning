CREATE SEQUENCE lib_users_seq;

CREATE TABLE lib_users(
   id integer NOT NULL DEFAULT nextval('lib_users_seq'),
   username VARCHAR,
   password VARCHAR,
   role varchar
);

ALTER SEQUENCE lib_users_seq
OWNED BY lib_users.id;

ALTER SEQUENCE lib_users_seq
	INCREMENT BY 50;


create  table counter (count integer);

CREATE SEQUENCE lib_news_seq;
	
CREATE TABLE lib_news(
   id integer NOT NULL DEFAULT nextval('lib_news_seq'),
   header VARCHAR,
   text VARCHAR
);
	
ALTER SEQUENCE lib_news_seq
	OWNED BY lib_news.id;
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button'
import {useRef, useState} from "react";

const AddPostDialog = (props) => {

    const handleClose = (event) => {
        props.onClose(event);
    }

    const handleChange = (event) => {
        const header = document.getElementById("header").value;
        const text = document.getElementById("text").value;
        props.onSubmit(header, text);
    }

    return(
        <div>
            <Dialog open={props.open} onClose={handleClose}>
                <DialogTitle>Добавление новостного поста</DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        id="header"
                        label="Заголовок"
                        fullWidth
                        variant="standard"
                    />
                    <TextField
                        margin="dense"
                        id="text"
                        label="Содержание поста"
                        fullWidth
                        multiline
                        rows={6}
                        variant="standard"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Отменить</Button>
                    <Button onClick={handleChange}>Добавить</Button>
                </DialogActions>
            </Dialog>
        </div>
    )

}

export default AddPostDialog;
import * as React from 'react';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import "../../style/mui/BasicPopover.css";

export default function BasicPopover(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);

    return (
        <div>
            <button className="popover-button" onClick={handleClick}>
                <span className="line" style={{marginTop: 0}}></span>
                <span className="line"></span>
                <span className="line"></span>
            </button>
            <div>
                <Popover
                    open={open}
                    anchorEl={anchorEl}
                    anchorPosition={{
                        left: 50,
                        bottom: 30
                    }}
                    onClose={handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                >
                    <Typography sx={{ p: 3 }}>Количество посещений: {props.counter}<br/>Дата входа: {props.date}</Typography>
                </Popover>
            </div>
        </div>
    );
}

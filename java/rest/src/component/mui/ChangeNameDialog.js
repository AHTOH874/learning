import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button'
import {useRef, useState} from "react";

const ChangeNameDialog = (props) => {

    const textFieldRef = useRef();

    const handleClose = (event) => {
        props.onClose(event);
    }

    const handleChange = (event) => {
        const result = document.getElementById("name").value;
        props.onSubmit(event, result);
    }

    return(
        <div>
            <Dialog open={props.open} onClose={handleClose}>
                <DialogTitle>Изменение</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Изменить имя пользователя {props.user.username}
                    </DialogContentText>
                    <TextField
                        margin="dense"
                        id="name"
                        label="Имя пользователя"
                        type="email"
                        fullWidth
                        variant="standard"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Отменить</Button>
                    <Button onClick={handleChange}>Изменить</Button>
                </DialogActions>
            </Dialog>
        </div>
    )

}

export default ChangeNameDialog;
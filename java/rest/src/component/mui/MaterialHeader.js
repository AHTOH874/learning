import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {useNavigate} from "react-router-dom";
import axios from 'axios';
import users from "../page/Users";
import {useEffect, useState} from "react";

const MaterialHeader = (props) => {

    const navigate = useNavigate();

    const [anchorElUser, setAnchorElUser] = useState(null);
    const [role, setRole] = useState("user");
    const [avatar, setAvatar] = useState("")
    const [user, setUser] = useState(null)
    const [avatarLink, setAvatarLink] = useState("/");


    useEffect(() => {
        axios.get("http://localhost:8080/account")
            .then(response => {
                setUser(response.data);
                setRole(response.data.role);
            })
    }, [])

    useEffect(() => {
        if(user) {
            loadAvatarLink()
        }
    }, [user])

    const loadAvatarLink = () => {
        axios.get("http://localhost:8080/files/image/" + user.id, { responseType: 'blob' })
            .then(response => {
                const url = window.URL.createObjectURL(response.data);
                setAvatarLink(url)
            })
    }

    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const darkTheme = createTheme({
        palette: {
            mode: 'light',
            primary: {
                main: '#AA3456'
            },
        },
    });

    const handleMenuItemClick = (name) => {
        switch (name) {
            case "Выйти":
                logoutClick()
                break;
            case "Пользователи":
                usersClick()
                break;
            case "Новостные посты":
                newsClick()
                break;
            case "Профиль":
                profileClick()
                break;
        }
        handleCloseUserMenu()
    }

    const createAvatar = () => {
        if(props.avatar) {
            return <Avatar alt={props.username} src={props.avatar} />
        }
        if(avatarLink === "") {
            return <Avatar alt={props.username} src={"/"} />
        }
        return <Avatar alt={props.username} src={avatarLink} />
    }

    const logoutClick = () => {
        axios
            .post("http://localhost:8080/account/logout")
            .then(response => navigate("/index"))
    }

    const usersClick = () => {
        navigate('/users');
    }

    const newsClick = () => {
        navigate('/editor')
    }

    const profileClick = () => {
        navigate('/profile')
    }

    const settings = [ "Профиль" ]

    if(role === "admin") {
        settings.push("Пользователи")
        settings.push("Новостные посты")
    } else if(role === "moderator") {
        settings.push("Новостные посты")
    }
    settings.push("Выйти")

    return (
        <ThemeProvider theme={darkTheme}>
            <AppBar position="sticky">
                <Container maxWidth="xl" color="red">
                    <Toolbar disableGutters>
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href="#"
                            sx={{
                                mr: 2,
                                display: { xs: 'none', md: 'flex' },
                                fontFamily: 'Roboto',
                                fontWeight: 700,
                                letterSpacing: '.1rem',
                                color: 'inherit',
                                textDecoration: 'none',
                                flexGrow: 1
                            }}
                        >
                            <span onClick={() => navigate("/")}>UniverNewsSite</span>
                        </Typography>

                        <Box sx={{ flexGrow: 0 }}>
                            <Tooltip title="Открыть профиль">
                            <Typography
                                variant="p"
                                sx={{
                                    color: 'inherit',
                                    fontFamily: 'Roboto',
                                    fontWeight: 400,
                                    mr: '20px',
                                    cursor: 'pointer'
                                }}
                                onClick={profileClick}
                            >
                                {props.username}
                            </Typography>
                            </Tooltip>
                            <Tooltip title="Открыть меню">
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    {createAvatar()}
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((value) => (
                                    <MenuItem key={value} onClick={() => {handleMenuItemClick(value)}}>
                                        <Typography textAlign="center">{value}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
        </ThemeProvider>
    );
}
export default MaterialHeader;
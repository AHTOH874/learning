import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button'
import {useRef, useState} from "react";
import FileInput from "../element/FileInput";
const UploadFileDialog = (props) => {

    const [file, setFile] = useState(null)

    const handleClose = (event) => {
        props.onClose(event);
    }

    const handleChange = (event) => {
        if(file === null) {
            alert("Вы не загрузили файл")
            return;
        }
        props.onSubmit(file);
    }

    const handleInputChange = (files) => {
        if(files !== undefined && files.length > 0) {
            const loadedFile = files[0];
            if(loadedFile.name.split(".").pop() !== 'jpg') {
                alert("Можно загрузить файл только в формате JPEG")
                return false;
            }
            setFile(loadedFile);
            return true;
        }
        return false;
    }

    return(
        <div>
            <Dialog open={props.open} onClose={handleClose}>
                <DialogTitle>Загрузка фотографии</DialogTitle>
                <DialogContent>
                    <FileInput id="file" onFileLoad={handleInputChange}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Отменить</Button>
                    <Button onClick={handleChange}>Загрузить</Button>
                </DialogActions>
            </Dialog>
        </div>
    )

}

export default UploadFileDialog;
import {useEffect, useState} from "react";
import axios from "axios";
import MaterialHeader from "../mui/MaterialHeader";
import {Navigate, useNavigate} from "react-router-dom";
import "../../style/Users.css"

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import NativeSelect from '@mui/material/NativeSelect';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import CircularProgress from '@mui/material/CircularProgress'
import Button from '@mui/material/Button'
import ChangeNameDialog from "../mui/ChangeNameDialog";
import {validateUsername} from "../../util/Validator";

const Users = () => {

    const [username, setUsername] = useState();
    const [isLoaded, setLoaded] = useState(false);
    const [hasAccess, setAccess] = useState(false);
    const [users, setUsers] = useState([]);
    const [roles, setRoles] = useState([]);
    const [open, setOpen] = useState(false);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [dialogUser, setDialogUser] = useState({username: "", role: "", id: 0});

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    useEffect(() => {
        axios.get("http://localhost:8080/account")
            .then(response => {
                setUsername(response.data.username);
                if (response.data.role === "admin") {
                    setAccess(true)
                }
            })
        axios
            .get("http://localhost:8080/roles")
            .then(response => setRoles(response.data));
        axios
            .get("http://localhost:8080/users")
            .then(response => {
                setUsers(response.data);
            })
            .finally(() => setLoaded(true))
    }, [])

    const onRoleChange = (event, username) => {
        const user = users.find((item, index, array) => {
            return item.username.toLowerCase() === username.toLowerCase();
        });

        axios.patch("http://localhost:8080/users/" + user.id, {
                role: event.target.value
            }
        )
            .then(response => {
                setOpen(true);
            })
    }

    const handleNickNameClick = (event) => {
        const user = users.find((item, index, array) => {
            return item.username.toLowerCase() === event.currentTarget.innerText.toLowerCase();
        });
        setDialogUser(user);
        setDialogOpen(true);
    }

    const handleDialogClose = () => {
        setDialogOpen(false)
    }

    const onChangeUsername = (event, result) => {
        if(!validateUsername(result)) {
            return;
        }
        axios.patch("http://localhost:8080/users/" + dialogUser.id, {
                username: result,              
        })
        const user = users.find((item, index, array) => {
            return item.username === dialogUser.username;
        });
        user.username = result;
        setDialogOpen(false)
    }

    if(isLoaded && !hasAccess) {
        return <Navigate to="/"/>
    }

    if(isLoaded) {
        return (
            <div>
                <MaterialHeader username={username}/>
                <div className="users-root">
                    <div className="users-header">
                        <h2>Пользователи</h2>
                    </div>
                    <div className="users">
                        <ChangeNameDialog user={dialogUser} onSubmit={onChangeUsername} onClose={handleDialogClose} open={dialogOpen}/>
                        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}
                                  anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}>
                            <Alert onClose={handleClose} severity="success" sx={{width: '100%'}}>
                                Роль сохранена
                            </Alert>
                        </Snackbar>
                        <TableContainer component={Paper}>
                            <Table sx={{minWidth: '70vw'}} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Имя пользователя</TableCell>
                                        <TableCell align="right">Роль</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {users.map((row) => (
                                        <TableRow
                                            key={row.username}
                                            sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                        >
                                            <TableCell component="th" scope="row">
                                                {row.username === "admin" ? <p>{row.username}</p> : <Button onClick={handleNickNameClick}>{row.username}</Button>}
                                            </TableCell>
                                            <TableCell align="right">
                                                <NativeSelect disabled={row.username === "admin"} defaultValue={row.role}
                                                              onChange={(e) => onRoleChange(e, row.username)}>
                                                    {roles.map((role) => <option value={role}>{role}</option>)}
                                                </NativeSelect>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div style={{width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
                <CircularProgress />
            </div>
        )
    }

}

export default Users;
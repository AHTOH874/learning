import '../../style/Login.css'
import {useNavigate} from "react-router-dom"
import {useRef} from "react";
import axios from "axios";
import {validateUsername, validatePassword} from "../../util/Validator";

const Login = () => {

    const usernameRef = useRef();
    const passwordRef = useRef();
    const buttonRef = useRef();
    const navigate = useNavigate();

    const loginClick = () => {
        axios.post("http://localhost:8080/account/login", {
            username: usernameRef.current.value,
            password: passwordRef.current.value,
        })
            .then(response => {

                alert("Вы успешно авторизовались")
                navigate("/index");

            })
            .catch(error => {
                const response = error.response;
                if(response.data === "account_not_exists") {
                    alert("Такого аккаунта не существует")
                } else if(response.data === "wrong_password") {
                    alert("Неверный логин или пароль")
                }
            });
    }

    const registerClick = () => {
        let usernameInput = usernameRef.current;
        let passwordInput = passwordRef.current;

        if(
            !validateUsername(usernameInput.value) ||
            !validatePassword(passwordInput.value)
        ) {
            return;
        }

        axios.post("http://localhost:8080/account/register", {
            username: usernameInput.value,
            password: passwordInput.value,
        })
            .then(response => {

                alert("Вы успешно зарегистрировались")
                navigate("/index");

            })
            .catch(error => {
                const response = error.response;
                if(response.data === "account_already_exists") {
                    alert("Такой аккаунт уже существует")
                }
            });
    }

    const handleKeyUp = (event) => {
        if(event.key === "Enter") {
            loginClick();
        }
    }

    return (
        <div className="login-form" onKeyUp={handleKeyUp}>
            <h1>Авторизация</h1>
            <input ref={usernameRef} id="username" placeholder="Имя пользователя"/><br/>
            <input ref={passwordRef} id="password" type="password" placeholder="Пароль"/><br/>
            <button ref={buttonRef} className="login-button" onClick={loginClick}>Войти</button><br/>
            <a className="register-link" href="#" onClick={registerClick}>Зарегистрироваться</a>
        </div>
    )

}

export default Login;
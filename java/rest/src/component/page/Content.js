import '../../style/Content.css'
import axios from "axios";
import {useEffect, useState} from "react";
import ContentPost from "../element/ContentPost";
import BasicPopover from "../mui/BasicPopover";
import MaterialHeader from "../mui/MaterialHeader";

const Content = (props) => {

    const [counter, setCounter] = useState(1);
    const [date, setDate] = useState("01.01.1970 00:00");
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        axios
            .get("http://localhost:8080/info")
            .then(response => {
                let data = response.data;
                setCounter(data.counter);
                setDate(data.date);
            })
        axios
            .get("http://localhost:8080/news")
            .then(response => {
                setPosts(response.data)
            })
    }, [])

    return (
        <div>
            <MaterialHeader username={props.username}/>
            <div className="content">
                <BasicPopover counter={counter} date={date}/>
                {posts.map(item => <ContentPost item={item}/>)}
            </div>
        </div>
    )

}

export default Content;
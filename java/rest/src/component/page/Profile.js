import "../../style/Profile.css"
import Avatar from '@mui/material/Avatar';
import {useEffect, useReducer, useState} from "react";
import axios from "axios";
import MaterialHeader from "../mui/MaterialHeader";
import {useNavigate} from "react-router-dom";
import UploadFileDialog from "../mui/UploadFileDialog";

const Profile = () => {

    const [role, setRole] = useState("user")
    const [username, setUsername] = useState("")
    const [user, setUser] = useState(null)
    const [open, setOpen] = useState(false)
    const navigate = useNavigate();
    const [avatarLink, setAvatarLink] = useState("/");

    useEffect(() => {
        axios.get("http://localhost:8080/account")
            .then(response => {
                setUsername(response.data.username);
                setRole(response.data.role);
                setUser(response.data);
            })
            .catch(error => {
                navigate("/")
            })
        
    }, [])

    useEffect(() => {
        if(user) {
            loadAvatarLink()
        }
    }, [user])

    const loadAvatarLink = () => {
        axios.get("http://localhost:8080/files/image/" + user.id, { responseType: 'blob' })
            .then(response => {
                const url = window.URL.createObjectURL(response.data);
                setAvatarLink(url)
            })
    }

    const formatRole = (role) => {
        switch (role) {
            case "admin":
                return { name: "Администратор", color: '#f17b7b' }
            case "moderator":
                return { name: "Модератор", color: '#9bff93' }
            case "user":
                return { name: "Пользователь", color: '#c7c4c4' }
        }
    }

    const logoutClick = () => {
        axios
            .post("http://localhost:8080/account/logout")
            .then(response => navigate("/index"))
    }

    const formattedRole = formatRole(role);

    const handleFileUpload = (file) => {
        const data = new FormData()
        data.append('file', file)
        let url = "http://localhost:8080/files/image";
        axios.post(url, data)
            .then(res => {
                loadAvatarLink()
            })
        setOpen(false)
    }

    const handleFileDialogClose = () => {
        setOpen(false)
    }

    const handleOpenDialog = () => {
        setOpen(true)
    }

    const handleDeleteImage = () => {
        axios.delete("http://localhost:8080/files/image")
            .then(res => {
                setAvatarLink("/")
            })
    }

    const renderAvatar = () => {
        return <Avatar sx={{width: '200px', height: '200px'}} alt={username} src={avatarLink} />
    }

    return (
        <div>
            <UploadFileDialog open={open} onSubmit={handleFileUpload} onClose={handleFileDialogClose}/>
            <MaterialHeader avatar={avatarLink} username={username}/>
            <div className="head-background"></div>
            <div style={{ display: 'flex', marginTop: '-100px', marginLeft: '100px' }}>
                {renderAvatar()}
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '90px', marginLeft: '20px'}}>
                    <p className="username">{username}</p>
                    <p className="role" style={{background: formattedRole.color}}>{formattedRole.name}</p>
                </div>
            </div>
            <div className="toolbar">
                <button className="upload-image toolbar-button" onClick={handleOpenDialog}>Загрузить фото</button>
                <button className="toolbar-button" onClick={handleDeleteImage}>Удалить фото</button>
                <button className="logout-button toolbar-button" onClick={logoutClick}>Выйти</button>
            </div>
        </div>
    )

}

export default Profile;
import { useEffect, useState } from "react";
import axios from "axios";
import { Navigate } from "react-router-dom";
import MaterialHeader from "../mui/MaterialHeader";
import BasicPopover from "../mui/BasicPopover";
import ContentPost from "../element/ContentPost";
import "../../style/Content.css"
import EditablePost from "../element/EditablePost";
import AddPostDialog from "../mui/AddPostDialog";
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import "../../style/NewsEditor.css"
import EditPostDialog from "../mui/EditPostDialog";

const NewsEditor = () => {

    const [username, setUsername] = useState();
    const [isLoaded, setLoaded] = useState(false);
    const [hasAccess, setAccess] = useState(false);
    const [news, setNews] = useState([])
    const [loadedNews, setLoadedNews] = useState([])
    const [addDialogOpen, setAddDialogOpen] = useState(false)
    const [editDialogOpen, setEditDialogOpen] = useState(false)
    const [open, setOpen] = useState(false)
    const [currentPost, setCurrentPost] = useState({})

    useEffect(() => {
        axios.get("http://localhost:8080/account")
            .then(response => {
                setUsername(response.data.username);
                if (response.data.role === "admin" || response.data.role === "moderator") {
                    setAccess(true)
                }
            }).finally(() => setLoaded(true))
        axios.get("http://localhost:8080/news")
            .then(response => setNews(response.data))
    }, [])

    useEffect(() => {
        setLoadedNews(news)
    }, [news])

    if (isLoaded && !hasAccess) {
        return <Navigate to="/" />
    }

    const handleDeletePost = (item) => {
        axios.delete("http://localhost:8080/news/" + item.id)
            .then(response => {
                setNews(response.data)
                setOpen(true)
            })
    }

    const handleOpenEditDialog = (user) => {
        setCurrentPost(user)
        setEditDialogOpen(true)
    }

    const handleEditPost = (id, header, text) => {
        const json = JSON.stringify({
            id: id,
            header: header,
            text: text
        })
        axios.patch("http://localhost:8080/news/" + id, json, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            setNews(res.data)
        });
        setEditDialogOpen(false)
    }

    const handleCloseEditDialog = () => {
        setEditDialogOpen(false)
        setCurrentPost({})
    }

    const handleAddPost = (header, text) => {
        axios.post("http://localhost:8080/news", {
            header: header,
            text: text
        }).then(res => {
            setNews(res.data)
        });

        setAddDialogOpen(false)
    }

    const handleOpenDialog = () => {
        setAddDialogOpen(true)
    }

    const handleCloseDialog = () => {
        setAddDialogOpen(false)
    }

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <div>
            <MaterialHeader username={username} />

            <div className="content">
                <div style={{ width: "700px", marginBottom: "-20px" }}><h1 style={{ color: "#AA3456" }}>Редактирование записей</h1></div>
                <AddPostDialog open={addDialogOpen} onSubmit={handleAddPost} onClose={handleCloseDialog} />
                <EditPostDialog post={currentPost} open={editDialogOpen} onSubmit={handleEditPost} onClose={handleCloseEditDialog} />
                <button className="add-post" onClick={handleOpenDialog}>
                    +
                </button>
                <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                    <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                        Пост удален
                    </Alert>
                </Snackbar>
                {loadedNews.length === 0 ? <p style={{ marginTop: "25%", fontFamily: "Roboto", fontSize: "20pt" }}>Записей нет</p> : loadedNews.map(item => <EditablePost onEditPost={handleOpenEditDialog} onDeletePost={handleDeletePost} item={item} />)}
            </div>
        </div>
    )

}

export default NewsEditor;
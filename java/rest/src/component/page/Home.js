import {useEffect, useState} from "react";
import axios from "axios";
import Content from "./Content";
import Login from "./Login";

axios.defaults.withCredentials = true;

const Home = () => {

    const [isLoaded, setLoaded] = useState(false);
    const [isLogged, setLogged] = useState(false);
    const [username, setUsername] = useState();

    useEffect(() => {
        axios.get("http://localhost:8080/account")
            .then(response => {
                setLogged(true);
                setUsername(response.data.username);
            })
            .finally(() => setLoaded(true))
    });

    if(isLoaded) {

        return isLogged ? <Content username={username}/> : <Login/>

    }

    return <div></div>

}

export default Home;
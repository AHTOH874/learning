import '../../style/ContentPost.css'
const ContentPost = ({item}) => {

    return (
        <div className="post">
            <h3 className="head">{item.header}</h3>
            <p className="text">{item.text}</p>
        </div>
    )

}

export default ContentPost;
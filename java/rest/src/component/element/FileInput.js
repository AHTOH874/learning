import "../../style/FileInput.css"
import {useRef} from "react";

const FileInput = (props) => {

    const labelRef = useRef();

    const { id, onFileLoad } = props;

    const onLoad = (event) => {
        const result = onFileLoad(event.target.files);
        if(result === true) {
            labelRef.current.innerText = event.target.files[0].name;
        }
    }

    return (
        <div className="file-input">
            <input type="file" id={id} onChange={onLoad}/>
            <label ref={labelRef} htmlFor={id}>Загрузить файл</label>
        </div>
    )

}

export default FileInput;
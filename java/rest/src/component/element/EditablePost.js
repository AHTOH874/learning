import "../../style/ContentPost.css"
import "../../style/EditablePost.css"

const EditablePost = (props) => {

    const item = props.item;

    const handleEditPost = (user) => {
        props.onEditPost(user)
    }

    const handleDeletePost = (user) => {
        props.onDeletePost(user);
    }

    return (
        <div className="post">
            <div style={{ display: "flex", flexDirection: 'row', marginBottom: "-10px" }}>
                <button className="edit-button" onClick={(e) => handleEditPost(item)}>Редактировать</button>
                <button style={{marginLeft: "10px"}} className="edit-button" onClick={(e) => handleDeletePost(item)}>Удалить</button>
            </div>
            <h3 className="head">{item.header}</h3>
            <p className="text">{item.text}</p>
        </div>
    )

}

export default EditablePost;
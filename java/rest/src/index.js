import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Routes, Navigate} from "react-router-dom";
import Users from "./component/page/Users";
import NewsEditor from "./component/page/NewsEditor";
import Profile from "./component/page/Profile";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
      <Routes>
          <Route path="/" element={<App/>}/>
          <Route path="/index" element={<Navigate to="/"/>}/>
          <Route path="/users" element={<Users/>}/>
          <Route path="/editor" element={<NewsEditor/>}/>
          <Route path="/profile" element={<Profile/>}/>
      </Routes>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

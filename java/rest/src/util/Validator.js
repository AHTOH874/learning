export function validateUsername(username) {
    if(username.length < 3) {
        alert("Длина имени пользователя должна быть не менее трех символов");
        return false;
    }
    if(username.length > 16) {
        alert("Длина имени пользователя должна быть не более 16 символов");
        return false;
    }
    if(!username.match("^[A-Za-z0-9_.-]+$")) {
        alert("Имя пользователя может состоять только из букв латинского алфавита, чисел и символов '_, ., -'")
        return false;
    }
    if(!username.match("^[A-Za-z0-9].+$")) {
        alert("Имя пользователя должно начинаться с буквы латинского алфавита или цифры")
        return false;
    }
    return true;
}

export function validatePassword(password) {
    if(password.length < 3) {
        alert("Длина пароля должна быть не менее трех символов");
        return false;
    }
    if(password.length > 50) {
        alert("Длина пароля должна быть не более 50 символов");
        return false;
    }
    if(!password.match("^[\u0410-\u042f\u0430-\u044f\u0401\u0451A-Za-z0-9_.&-]+$")) {
        alert("Пароль может состоять только из букв латинского алфавита и кириллицы, чисел и символов '_, ., -, &'")
        return false;
    }
    return true;
}
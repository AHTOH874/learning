package ru.pnzgu.moipvm;

public class Bus extends AutoClass {
    private int seats;
    private String bodyType;

    public Bus(String brand, String model, int year, String licensePlate, double mileage, int seats, String bodyType) {
        super(brand, model, year, licensePlate, mileage);
        this.seats = seats;
        this.bodyType = bodyType;
    }

    public void openDoors() {
        // код для открытия дверей
    }

    public void closeDoors() {
        // код для закрытия дверей
    }

    public double calculateFuelConsumption() {
        // код для расчета расхода топлива
        return 0.0;
    }

    public void controlAirConditioner() {
        // код для управления кондиционером
    }
}
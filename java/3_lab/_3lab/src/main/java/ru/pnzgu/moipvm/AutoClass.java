package ru.pnzgu.moipvm;

public class AutoClass {
    private String brand;
    private String model;
    private int year;
    private String licensePlate;
    private double mileage;

    public AutoClass(String brand, String model, int year, String licensePlate, double mileage) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.licensePlate = licensePlate;
        this.mileage = mileage;
    }

    public void startEngine() {
        // код для запуска двигателя
    }

    public void stopEngine() {
        // код для остановки двигателя
    }

    public void controlSpeed(double speed) {
        // код для управления скоростью
    }
}

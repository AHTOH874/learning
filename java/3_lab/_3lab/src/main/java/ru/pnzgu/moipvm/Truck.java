package ru.pnzgu.moipvm;

public class Truck extends AutoClass {
    private int carryingCapacity;
    private String bodyType;

    private boolean platformIsUp;

    public Truck(String brand, String model, int year, String licensePlate, double mileage, int carryingCapacity, String bodyType) {
        super(brand, model, year, licensePlate, mileage);
        this.carryingCapacity = carryingCapacity;
        this.bodyType = bodyType;
        this.platformIsUp = false;
    }

    public void loadCargo() {
        // код для загрузки груза
    }

    public void unloadCargo() {
        // код для выгрузки груза
    }

    public double calculateFuelConsumption() {
        // код для расчета расхода топлива
        return 0.0;
    }

    public void controlLiftPlatform() {
        // код для управления подъемной платформой
        if (this.platformIsUp){
            this.platformIsUp = false;
        } else {
            this.platformIsUp = true;
        }
    }
}
package ru.pnzgu.fvt.moipvm;

import java.util.Scanner;
import java.util.regex.Pattern;

// The standard (IEEE 802) format for printing MAC-48 addresses in human-friendly form is six groups of two hexadecimal digits, separated by hyphens - or colons :.


public class MainClass {
    /**
     * Осуществляется поиск значения в Input по Pattern, если успешно, выводится сообщение о том, что удовлетворяет паттерну
     * @param pattern - String Regex pattern 
     * @param input - String Строка по которой осуществляется поиск паттерна
     */
    public static void prettyPrint(String pattern, String input ) {
        if (Pattern.matches(pattern, input)) {
            System.out.println("Введёная строка является MAC-адрессом");
        } else {
            System.out.println("Введёная строка не является MAC-адрессом");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("3 бригада.");
        System.out.println(
                "Написать регулярное выражение, определяющее является ли заданная строка правильным MAC-адресом");
        
        String pattern = "^(?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2})$";
        
        while (true) {
            System.out.print("Mac adress: ");

            if (!scanner.hasNextLine()) {
                System.out.println("Все введённые строки обработаны..");
                break;
            }

            String input = scanner.nextLine();

            prettyPrint(pattern, input);

        }
        scanner.close();

    }
}

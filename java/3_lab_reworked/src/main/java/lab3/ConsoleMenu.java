package lab3;

import lab3.generator.AutoGenerator;
import lab3.model.Auto;
import lab3.service.AutoService;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ConsoleMenu {

    private final Scanner scanner = new Scanner(System.in);
    private final AutoService autoService;

    public ConsoleMenu(AutoService autoService) {
        this.autoService = autoService;
    }

    public void run() {
        handleUserInput();
    }

    private void handleUserInput() {
        while(true) {
            showMenu();
            if(scanner.hasNextInt()) {
                int action = scanner.nextInt();
                scanner.nextLine();
                switch (action) {
                    case 1:
                        handleShowAllAction();
                        break;
                    case 2:
                        handleAddAction();
                        break;
                    case 3:
                        handleRemoveAction();
                        break;
                    case 4:
                        handleChangePlateAction();
                        break;
                    case 5:
                        handleSortingAutosAction();
                        break;
                    case 6:
                        handleFindingCheapAuto();
                        break;
                    case 7:
                        handleFindingMaxMileageForOlderAuto();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Неизвестное действие");
                }
            } else {
                System.out.println("Неизвестное действие");
                scanner.nextLine();
            }
        }
    }

    private void handleShowAllAction() {
        System.out.println("Все автомобили:");
        List<Auto> autos = autoService.findAll();
        if(autos.size() == 0) {
            System.out.println("Вы не добавили ни одной горы");
        } else {
            printAutoList(autos);
        }
    }

    private void handleAddAction() {
        System.out.println("Введите номер автомобиля:");
        String plate = scanner.nextLine();
        Auto auto = AutoGenerator.generateAuto(plate);
        autoService.addAuto(auto);
        System.out.println("Добавлен новый автомобиль");
        System.out.println(auto);
    }

    private void handleRemoveAction() {
        System.out.println("Введите номер автомобиля:");
        String plate = scanner.nextLine();
        boolean removed = autoService.removeAuto(plate);
        if(removed) {
            System.out.println("Вы удалили автомобиль с номером '" + plate + "'");
        } else {
            System.out.println("Автомобиль с номером '" + plate + "' не найдено");
        }
    }

    private void handleChangePlateAction() {
        System.out.println("Введите номер автомобиля:");
        String plate = scanner.nextLine();
        Auto auto = autoService.findAuto(plate);
        if(auto == null) {
            System.out.println("Авто с номером '" + plate + "' не найдено");
        } else {
            System.out.println("Введите новое номер автомобиля:");
            String newPlate = scanner.nextLine();
            auto.setPlate(newPlate);
            System.out.println("Обновленная информация о автомобиле:");
            System.out.println(auto);
        }
    }

    private void handleSortingAutosAction() {
        System.out.println("Отсортированный список автомобилей:");
        autoService.findAll().stream()
                .sorted(Comparator.comparingInt(Auto::getBirthyear).reversed())
                .forEach(System.out::println);
    }

    private void handleFindingCheapAuto() {
        Auto auto = autoService.findCheapAuto();
        if(auto == null) {
            System.out.println("Вы не добавили ни одного автомобиля");
        } else {
            System.out.println("Самая дешёвая машина:");
            System.out.println(auto);
        }
    }

    private void handleFindingMaxMileageForOlderAuto() {
        Auto auto = autoService.findMaxMileageForOlderAuto();
        if(auto == null) {
            System.out.println("Не нашлось ни одного автомобиля, подходящей под эти условия");
        } else {
            System.out.println("Максимальный пробег для машины старше 3-х лет представлен в данном автомобиле:");
            System.out.println(auto);
        }
    }

    private void printAutoList(List<Auto> autos) {
        for(Auto auto : autos) {
            System.out.print(auto);
        }
    }

    private static void showMenu() {
        System.out.println("Меню пользователя:");
        System.out.println("1 - Вывести все авто на экран");
        System.out.println("2 - Добавить новый автомобиль");
        System.out.println("3 - Удалить авто");
        System.out.println("4 - Изменить номер автомобиля");
        System.out.println("5 - Вывести отсортированный список автомобилей");
        System.out.println("6 - Найти самую дешёвую машину");
        System.out.println("7 - Найти автомобиль с максимальным пробегом, старше 3-х лет");
        System.out.println("0 - Завершить выполнение программы");
        System.out.println();
    }

}

package lab3;

import lab3.generator.AutoGenerator;
import lab3.model.Auto;
import lab3.service.AutoService;

import java.util.List;

public class Application {

    public static void main(String[] args) {
        List<Auto> autos = AutoGenerator.generateAutos(6);
        AutoService autoService = new AutoService(autos);
        ConsoleMenu consoleMenu = new ConsoleMenu(autoService);
        consoleMenu.run();
    }

}

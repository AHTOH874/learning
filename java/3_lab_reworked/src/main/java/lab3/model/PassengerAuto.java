package lab3.model;

import lab3.model.Auto;

public class PassengerAuto extends Auto {

    private String typeName = "Межгород";
    private double countOfPassengers = 0;
    private double countOfDtp= 0;

    @Override
    public String getType() { return "Пассажирский"; }


    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public double getCountOfPassengers() {
        return countOfPassengers;
    }

    public void setCountOfPassengers(double countOfPassengers) {
        this.countOfPassengers = countOfPassengers;
    }

    public double getCountOfDtp() {
        return countOfDtp;
    }

    public void setCountOfDtp(double countOfDtp) {
        this.countOfDtp = countOfDtp;
    }

    @Override
    public  String toString() {
        return super.toString() +
                String.format(" направление: %s, допустимое кол-во пассажиров: %.2f, кол-во дтп: %.2f",
                        typeName,
                        countOfPassengers,
                        countOfDtp
                );
    }
}

package lab3.model;

public class Bus  extends PassengerAuto {

    private double number = 17;
    private double rate = 0.5; // кол-во в час по направлению

    @Override
    public String getType() { return "Автобус"; }


    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return super.toString() +
                String.format("  номер маршрута: %.2f, частота проезда: %s%n",
                        number,
                        rate
                );
    }
}

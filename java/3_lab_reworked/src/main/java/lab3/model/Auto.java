package lab3.model;

public class Auto {

    private String brand = "brand";
    private String model = "model";
    private String plate = "";
    private double price = 0;
    private Integer birthyear = 0;
    private double mileage = 0;

    public String getType() { return  "Автомобиль"; }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getBirthyear() {
        return birthyear;
    }

    public void setBirthyear(Integer birthyear) {
        this.birthyear = birthyear;
    }

    @Override
    public String toString(){
        return String.format("* %s, Бренд: %s, Модель: %s, Номер: %s, Пробег: %.2f, Год выпуска %04d, Цена: %.2f%n",
                getType(),
                brand,
                model,
                plate,
                mileage,
                birthyear,
                price
        );
    }
}

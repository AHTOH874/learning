package lab3.generator;

import lab3.model.Auto;
import lab3.model.PassengerAuto;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AutoGenerator {

    private static final Random RANDOM = new Random();

    public static Auto generateAuto(String plate) {
        Auto auto = new Auto();
        auto.setPlate(plate);
        setRandomDataToAuto(auto);
        return auto;
    }

    public static PassengerAuto generatePassengerAuto(String typeName) {
        PassengerAuto passAuto = new PassengerAuto();
        passAuto.setTypeName(typeName);
        setRandomDataToAuto(passAuto);
        passAuto.setCountOfDtp(RANDOM.nextInt(100));
        passAuto.setCountOfPassengers(RANDOM.nextInt(50));
        return passAuto;
    }

    public static List<Auto> generateAutos(int count) {
        List<Auto> autos = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            if (RANDOM.nextBoolean()) {
                autos.add(generateAuto(generateRandomPlate()));
            } else {
                autos.add(generatePassengerAuto("Межгород " + RANDOM.nextInt(30)));
            }
        }
        return autos;
    }

    private static void setRandomDataToAuto(Auto auto) {
        auto.setBirthyear(RANDOM.nextInt(3000));
        auto.setPrice(RANDOM.nextInt(10000));
        auto.setMileage(RANDOM.nextInt(1000000));
        auto.setBrand(generateRandomBrand());
        auto.setModel(generateRandomModel());
    }

    private static String generateRandomPlate() { return " " + RANDOM.nextInt(1000); }
    private static String generateRandomModel() { return "model " + RANDOM.nextInt(10); }
    private static String generateRandomBrand() { return "brand " + RANDOM.nextInt(10); }

}

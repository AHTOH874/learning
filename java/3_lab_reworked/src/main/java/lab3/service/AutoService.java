package lab3.service;

import lab3.model.Auto;

import java.time.Year;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AutoService {

    private final List<Auto> autos;

    public AutoService(List<Auto> autos) {
        this.autos = autos;
    }

    public void addAuto(Auto auto) {autos.add(auto); }

    public boolean removeAuto(String plate) {
        return autos.removeIf(auto -> auto.getPlate().equalsIgnoreCase(plate));
    }

    public List<Auto> findAll() {
        return autos;
    }

    public Auto findAuto(String plate) {
        return autos.stream()
                .filter(auto -> auto.getPlate().equalsIgnoreCase(plate))
                .findFirst()
                .orElse(null);
    }

    public Auto findCheapAuto() {
        return autos.stream().min(Comparator.comparingDouble(Auto::getPrice)).orElse(null);
    }

    public Auto findMaxMileageForOlderAuto() {
        return autos.stream()
                .filter(auto -> (Year.now().getValue() - auto.getBirthyear()) >= 3)
                .max(Comparator.comparingDouble(Auto::getMileage))
                .orElse(null);
    }


}

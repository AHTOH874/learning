package ru.pnzgu.fvt.moipvm;

import java.util.Scanner;


public class MainClass {

    public static final String[] BELOW_TWENTY = { "ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь",
            "восемь", "девять", "десять", "одинадцать", "двенадцадь", "тринадцать", "четырнадцать", "пятнадцать",
            "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" };
    public static final String[] TENS = { "сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят",
            "семьдесят", "восемьдесят", "девяносто" };
    public static final String[] HUNDREDS = { "одна тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот",
            "восемьсот", "девятьсот" };
    public static final String[] THOUSANDS = { "одна тысяча" };

    public static String getTensString(int number) {
        int high = number / 10;
        int low = number % 10;
        String text = TENS[high];
        if (low != 0)
            text = text + " " + BELOW_TWENTY[low];
        return text;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("3 бригада.");
        System.out.println(
                "Для произвольного числа от 0 до 1000 вывести на консоль ее значение прописью. Например, для числа 9 на консоли должна быть напечатана строка «девять».");

        while (true) {
            System.out.print("Число от 0 до 1000: ");

            if (scanner.hasNext("exit"))
                break;

            if (!scanner.hasNextInt()) {
                System.out.println("Введёное значение не число. Попробуй ещё раз, запустив заново приложение...");
                break;
            }

            int number = scanner.nextInt();

            if (number < 0 || number > 1000) {
                System.out.println("Попробуй ещё раз...");
                continue;
            }

            if (number < 20)
                System.out.println(BELOW_TWENTY[number]);
            else if (number < 100) {
                System.out.println(getTensString(number));
            } else if (number < 1000) {
                int high = number / 100;
                int low = number % 100;
                String text = HUNDREDS[high];
                if (low < 20 && low != 0) {
                    text = text + " " + BELOW_TWENTY[low];
                } else if (low != 0) {
                    text = text + " " + getTensString(low);
                }
                System.out.println(text);
            } else
                System.out.println(THOUSANDS[0]);
        }
        scanner.close();

    }
}

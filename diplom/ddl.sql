create table jkh.service (
    id_service int,
    name_of_service varchar, 
    price int,
    name_of_value varchar,
    id_organization int,
    primary key (id_service)
) distributed by (id_service);

create table jkh.organization (
    id_organization int,
    type_organization varchar,
    name_of_organization varchar,
    primary key (id_organization)
) distributed by (id_organization);

create table jkh.worker (
    id_worker int,
    secondname varchar,
    firstname varchar,
    patronymic varchar,
    position varchar,
    id_organization int,
    primary key (id_worker)
) distributed by (id_worker);

create table jkh.user (
    id_user int,
    secondname varchar,
    firstname varchar,
    patronymic varchar,
    adress varchar
    primary key (id_user)
) distributed by (id_user);

create table jkh.index (
    id_index int,
    value int,
    adress varchar,
    d_date date,
    id_service int,
    id_user int,
    primary key (id_index)
) distributed by (id_index);

create table jkh.appeal (
    id_appeal int,
    text varchar,
    d_date date,
    id_user int,
    id_worker int,
    status_of_appeal varchar
    primary key (id_appeal)
) distributed by (id_appeal);


(1, 500, 'г. Пенза, ул. Пушкина, д. 4, кв. 5', date 2023-03-07, 1);
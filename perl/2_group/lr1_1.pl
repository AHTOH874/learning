#!/usr/bin/perl -w
 
use Tk;
use strict;
use Net::Ping;
 
my $mw = MainWindow->new;
$mw->geometry("500x200");
$mw->title("Lab10 ping");
 
 
my $main_frame = $mw->Frame()->pack(-side => 'top', -fill => 'x');
 
my $top_frame = $main_frame->Frame(-background => "red")->pack(-side => 'top',-fill => 'x');

my $left_frame = $main_frame->Frame(-background => "black")->pack(-side => 'left', -fill => 'y');

my $right_frame = $main_frame->Frame(-background => "white")->pack(-side => "right");
 
$top_frame->Label(-text => "Ping", -background => "red")->pack(-side => "top");

$left_frame->Label(-text => "Address: ", -background => "black", -foreground => "yellow")->pack(-side => "left");

my $copy_entry = $left_frame->Entry(-background => "white", -foreground => "red")->pack(-side => "left");

my $copy_button = $left_frame->Button(-text => "Ping!", -command => \&ping)->pack(-side => "right");
 

my $paste_text = $right_frame->Text(-background => "white", -foreground => "black")->pack(-side => "top");
 
sub ping {
    $paste_text->delete('0.0', 'end');
    my $host = $copy_entry->get();
    my $p=Net::Ping->new()
    or die "Can\’t create new ping object:$!\n";
    if($p->ping($host)){
        $paste_text->insert("end", "$host is alive");
    } else {
        $paste_text->insert("end", "$host is not alive");
    }

    $p->close();
}

 
MainLoop;
#!/usr/bin/perl -w
 
use Tk;
use strict;
use Net::Ping;
use LWP::UserAgent;
 
my $mw = MainWindow->new;
$mw->geometry("500x200");
$mw->title("Lab10 agent");
 
 
my $main_frame = $mw->Frame()->pack(-side => 'top', -fill => 'x');
 
my $top_frame = $main_frame->Frame(-background => "red")->pack(-side => 'top',-fill => 'x');

my $left_frame = $main_frame->Frame(-background => "black")->pack(-side => 'left', -fill => 'y');

my $right_frame = $main_frame->Frame(-background => "white")->pack(-side => "right");
 
$top_frame->Label(-text => "Agent", -background => "red")->pack(-side => "top");

$left_frame->Label(-text => "Address: ", -background => "black", -foreground => "yellow")->pack(-side => "left");

my $copy_entry = $left_frame->Entry(-background => "white", -foreground => "red")->pack(-side => "left");


my $close_button = $left_frame->Button(-text => "Close", -command => sub{exit})->pack(-side => "bottom",-fill => 'y');
my $copy_get_file = $left_frame->Button(-text => "Get to the file", -command => \&get_file)->pack(-side => "bottom",-fill => 'y');
my $copy_get = $left_frame->Button(-text => "Get", -command => \&get)->pack(-side => "bottom",-fill => 'y');
my $copy_get_info = $left_frame->Button(-text => "Get info", -command => \&get_info)->pack(-side => "bottom",-fill => 'y');
 

my $paste_text = $right_frame->Text(-background => "white", -foreground => "black")->pack(-side => "top");
 
sub get_info {
    $paste_text->delete('0.0', 'end');
    my $ua=LWP::UserAgent->new;$ua->agent("MSIE 7.0 ");
    my $url= $copy_entry->get();
    my $req=HTTP::Request->new(GET =>$url); 
    $req->header('Accept'=>'text/html');
    my $res=$ua->request($req);
    my @res=$res->headers->as_string; 
    $paste_text->insert("end", @res);
}

sub get{
    $paste_text->delete('0.0', 'end');
   my $ua=LWP::UserAgent->new;$ua->agent("MSIE 7.0 ");
   my $url= $copy_entry->get();
   my $req=HTTP::Request->new(GET =>$url); 
   $req->header('Accept'=>'text/html');
   my $res=$ua->request($req);
   my @res=$res->content; 
    $paste_text->insert("end", @res);
}

sub get_file {
    my $url= $copy_entry->get();
    my $filePath = "./log.txt";
    open(my $fh, '>', $filePath) or die "Ошибка открытия файла!: $!\n";
   my $ua=LWP::UserAgent->new;$ua->agent("MSIE 7.0 ");
   my $req=HTTP::Request->new(GET =>$url); 
   $req->header('Accept'=>'text/html');
   my $res=$ua->request($req);
   my @res=$res->content; 
   print $fh @res;
   close $fh;
}
 
MainLoop;

#!/usr/bin/perl -w
 
use Tk;
use strict;
use Net::FTP;


my $ftp;


my $mw = MainWindow->new;
$mw->geometry("800x300");
$mw->title("Lab10 agent");
 
 
my $main_frame = $mw->Frame()->pack(-side => 'top', -fill => 'x');
 
my $top_frame = $main_frame->Frame(-background => "red")->pack(-side => 'top',-fill => 'x');

my $left_frame = $main_frame->Frame(-background => "black")->pack(-side => 'left', -fill => 'y');

my $right_frame = $main_frame->Frame(-background => "white")->pack(-side => "right");
 
$top_frame->Label(-text => "Agent", -background => "red")->pack(-side => "top");

$left_frame->Label(-text => "Address: ", -background => "black", -foreground => "yellow")->pack(-side => "left");

my $copy_entry = $left_frame->Entry(-background => "white", -foreground => "red")->pack(-side => "left");


my $close_button = $left_frame->Button(-text => "Close", -command => sub{exit})->pack(-side => "bottom",-fill => 'y');
my $disconnect_button = $left_frame->Button(-text => "Disconnect", -command => \&disconnect)->pack(-side => "bottom",-fill => 'y');
my $get_file_button = $left_frame->Button(-text => "Get file", -command => \&get_file)->pack(-side => "bottom",-fill => 'y');
my $go_button = $left_frame->Button(-text => "Go directory", -command => \&go_directory)->pack(-side => "bottom",-fill => 'y');
my $connect_button = $left_frame->Button(-text => "Connect", -command => \&connect)->pack(-side => "bottom",-fill => 'y');
 

my $paste_text = $right_frame->Text(-background => "white", -foreground => "black")->pack(-side => "top");
my $labelel = $right_frame->Label(-text => "Content", -background => "red")->pack(-side => "top");
 
sub connect {
    $paste_text->delete('0.0', 'end');
    my $url= $copy_entry->get();
    $ftp=Net::FTP->new($url, Debug => 7) or die "Can\'t connect:$@\n";   
    $ftp->login("anton", "power");
    my @files = $ftp->ls("./") or die "Can\'t login:$@\n";
    $paste_text->insert("end", @files);
}

sub go_directory {
    $paste_text->delete('0.0', 'end');
    my $directory= $copy_entry->get();
    $ftp->cwd($directory)or die "Can\'t login:$@\n";
    my @files = $ftp->ls("./") or die "Can\'t login:$@\n";
    $paste_text->insert("end", @files);
}

sub get_file {
    my $filename = $copy_entry->get();
    my $filePath = "./" . $filename;
 $ftp->get ($filename) or die "Can\'t get $filename:$@\n";
}

sub disconnect {
    $paste_text->delete('0.0', 'end');
    $ftp->close();
}
 
MainLoop;

#!/usr/bin/perl -w
use IO::Socket;
use Net::hostent;
use Sys::Hostname;
use POSIX;

use lib '/home/anton/projects/perl';
use protocol;
system("clear");

$pid = fork;


if($pid > 0){	
		printf "Разделение прошло успешно, новый pid: %d\n", $$;
		exit;

}
else{
		close STDIN;
		close STDOUT;
		close STDERR;
}

POSIX::setsid() 
    or die("Не могу создать POSIX сессию: $!");

$time_to_die = 0;
sub signal_handler {
 $time_to_die = 1;
}
$SIG{INT} = $SIG{TERM} = $SIG{HUP} = \&signal_handler;

until($time_to_die){
    $server = IO::Socket::INET->new(Proto => 'tcp',
        LocalPort => 8080,
        Listen => SOMAXCONN,
        Proto => 'tcp',
        Reuse => 1
    );

    die "Не могу запустить сервер" unless $server;

    while ($client = $server->accept()) {

        
        if ($pid = fork) {
        	next;
        }
        
        unless (defined $pid) {
        	die "fork: $!";
        } 
        
        $server->close();
        $pid = $$;


        my $clientMessage = "Добро пожаловать; введите \"help\" для списка комманд.
        Вас обслуживает процесс $pid\n";
        
        sendCMD($client,"msg", $clientMessage);
        

        while ($cmd = <$client>) {
    	
			($cmd, $msg) = getCMD($cmd);
			

			$cmd0 = $cmd;
			$cmd = convertComand($cmd);
			$_ = $cmd;

			if ($cmd == 0) {
				sendCMD($client,"msg", getHelp());
			}
			elsif ($cmd == 1) {
				sendCMD($client,"msg", getInfo($client));
			}
			elsif ($cmd == 2) {
				sendCMD($client,"msg", getDate());
			}
			elsif ($cmd == 3) {
				sendCMD($client,"msg", getTime());
			}
			elsif ($cmd == 4) { # load from client to server
				loadFile($client,"");
			}
			elsif ($cmd == 5) { # load from client to server convert to ROME
				loadFile($client,"toRome");
			}
			elsif ($cmd == 6) { # load from client to server convert to ARAB
				loadFile($client,"toArab");
			}
			elsif ($cmd == 7) { # download from server to client
				download($client);
			}
			else {
				sendCMD($client,"msg", "Некорректная команда $cmd0\n");
			}

		}

        close $client;
        printf "[Соединение закрыто: %s]\n", $client->peerhost;
        exit; 
    } continue {
        close $client;
    }
}

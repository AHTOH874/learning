#!/usr/bin/perl
use IO::Socket;
use Net::hostent;
use Sys::Hostname;

use lib '/home/user/Рабочий стол/9/new';
use protocol;
system("clear");



$server = IO::Socket::INET->new(Proto => 'tcp',
    LocalPort => 8081,
    Listen => SOMAXCONN,
    Proto => 'tcp',
    Reuse => 1
);

die "Не могу запустить сервер" unless $server;
print "[Сервер из файла $0 запущен]\n";

my $clientMessage = "Добро пожаловать. введите \"help\" для списка комманд.\n";
while ($client = $server->accept()) {
	
    printf "[Соединение из %s]\n", $client->peerhost;
    
    sendCMD($client,"msg", $clientMessage);

    while ($cmd = <$client>) {
    	
    	($cmd, $msg) = getCMD($cmd);
    	

    	$cmd0 = $cmd;
    	$cmd = convertComand($cmd);
    	$_ = $cmd;

    	if ($cmd == 0) {
    		sendCMD($client,"msg", getHelp());
    	}
    	elsif ($cmd == 1) {
    		sendCMD($client,"msg", getInfo($client));
    	}
    	elsif ($cmd == 2) {
    		sendCMD($client,"msg", getDate());
    	}
    	elsif ($cmd == 3) {
    		sendCMD($client,"msg", getTime());
    	}
    	elsif ($cmd == 4) { # load from client to server
    		loadFile($client,"");
    	}
    	elsif ($cmd == 5) { # load from client to server convert to ROME
    		loadFile($client,"toRome");
    	}
    	elsif ($cmd == 6) { # load from client to server convert to ARAB
    		loadFile($client,"toArab");
    	}
    	elsif ($cmd == 7) { # download from server to client
    		download($client);
    	}
    	else {
    		sendCMD($client,"msg", "Некорректная команда $cmd0\n");
    	}

    }

    close $client;
    printf "[Соединение закрыто: %s]\n", $client->peerhost;
    exit;
}


  

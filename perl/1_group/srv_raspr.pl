#!/usr/bin/perl

use IO::Socket;
use Socket;
use Sys::Hostname;
use POSIX;

use lib '/home/anton/projects/perl';
use protocol;

system("clear");

my $clientMessage = "Добро пожаловать. введите \"help\" для списка комманд.\n";

$SIG{INT} = sub
{
	close($serv);
	close($s1);
	close($s2);
	print "\nСервер завершает свою работу\n";
	exit;
};

sub server {
	my ($sock) = @_;
	
	$serv = IO::Socket::INET->new(
			LocalPort => $port + $sock,
			Type => SOCK_STREAM,
			Reuse => 1,
			Listen => SOMAXCONN) or die "$!\n";
	sleep 1;

	if($sock == 0)
	{
	
		
die "Не могу запустить сервер" unless $serv;
print "[Сервер из файла $0 запущен]\n";

		$s1 = IO::Socket::INET->new(
			PeerAddr => 'localhost',
			PeerPort => $port + 1,
			Proto => 'tcp',
			Type => SOCK_STREAM) or die "$!\n";
		
		$s2 = IO::Socket::INET->new(
			PeerAddr => 'localhost',
			PeerPort => $port + 2,
			Proto => 'tcp',
			Type => SOCK_STREAM) or die "$!\n";
	}

	NEWCLIENT:
	while($client = $serv->accept())
	{
		if($sock == 0) {
			print "Клиент подключился\n";
		    sendCMD($client, "msg", $clientMessage);
			while($cmd = <$client>) {
				($cmd, $msg) = getCMD($cmd);
    			
    			$cmd0 = $cmd;
    			$cmd = convertComand($cmd);
    			$_ = $cmd;
				
				if ($cmd == 0) {
					print "Отправка на сервер 1\n";
					sendCMD($s1, $cmd, "");
					$text = <$s1>;
					$text = replaceNL($text, 0); 
					($cmd, $msg) = getCMD($text);
					sendCMD($client, "msg", $msg);
    			} elsif ($cmd == 1) {
    				sendCMD($client, "msg", getInfo($client));
    			} elsif ($cmd == 2) {
					print "Отправка на сервер 2\n";
					sendCMD($s2, $cmd, "");
					$cmd = <$s2>;
					($cmd, $msg) = getCMD($cmd);
					sendCMD($client, "msg", $msg);
    			} elsif ($cmd == 3) {
					print "Отправка на сервер 2\n";
					sendCMD($s2, $cmd, "");
					$cmd = <$s2>;
					($cmd, $msg) = getCMD($cmd);
					sendCMD($client, "msg", $msg);
    			} elsif ($cmd == 4) { # load from client to server
    				loadFile($client,"");
    			} elsif ($cmd == 5) { # load from client to server convert to ROME
    				loadFile($client,"toRome");
    			} elsif ($cmd == 6) { # load from client to server convert to ARAB
    				loadFile($client,"toArab");
    			} elsif ($cmd == 7) { # download from server to client
    				download($client);
    			} else {
    				sendCMD($client,"msg", "Некорректная команда $cmd0\n");
    			}
			}
		} elsif($sock == 1) {
			while ($cmd = <$client>) {
				($cmd, $msg) = getCMD($cmd);
				if ($cmd == 0) {
					sendCMD($client, "msg", replaceNL(getHelp(), 1));
				}	
			}
		} elsif($sock == 2) {
			while ($cmd = <$client>) {
				($cmd, $msg) = getCMD($cmd);
				if ($cmd == 2) {
    				sendCMD($client,"msg", getDate());
    			} elsif ($cmd == 3) {
    				sendCMD($client,"msg", getTime());
    			}
    		}
		}
	}
}

$sock = 0;
$port = 8080;

for($i = 1; $i < 3; $i++)
{
	$pid = fork;
	
	if($pid == 0)
	{
		server($i);
	}
}

server(0);

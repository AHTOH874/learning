#!/usr/bin/perl -w
use IO::Socket;
use Net::hostent;
use Sys::Hostname;
use POSIX;


use lib '/home/user/Рабочий стол/9/new';

use protocol;

system("clear");


$server = IO::Socket::INET->new(Proto => 'tcp',
    LocalPort => 8081,
    Listen => SOMAXCONN,
    Proto => 'tcp',
    Reuse => 1
);

# Настройки:
$PREFORK = 5; # Кол-во потомков
$CLIENTS_PER_FORK = 1; # Кол-во клиентов на 1 потомке

%children = (); # Потомки

die "Не могу запустить сервер" unless $server;
print "[Сервер из файла $0 запущен]\n";

# REAPER чистит "мёртвых" потомков
sub REAPER {
    $SIG{CHLD} = \&REAPER;
    my $pid = wait;
    $children--;
    delete $children{$pid};
}

# HUNTSMAN убивает своих потомков
sub HUNTSMAN {
    local($SIG{CHLD}) = 'IGNORE';
    kill 'INT' => keys %children;
    exit;
}

# Создаём потомков
for (1..$PREFORK) {
    make_new_child();
}

# Установить обработчики сигналов
$SIG{CHLD} = \&REAPER;
$SIG{INT} =  \&HUNTSMAN;

# Поддерживаем численность процессов
while(1){
    sleep;

    for($i = $children; $i < $PREFORK; $i++){
        make_new_child();
    }
}

# Создание потомков
sub make_new_child {
    my $pid;
    my $sigset;

    $sigset = POSIX::SigSet->new(SIGINT);
    sigprocmask(SIG_BLOCK, $SIGSET)
        or die("Не могу заблокировать SIGINT для fork: $!\n");

    die "fork: $!" unless defined ($pid = fork);

    if ($pid) {
        printf "Создаю потомка, pid: %s\n", $pid;

        # Родитель запоминает потомка и завершает работу.
        sigprocmask(SIG_UNBLOCK, $sigset)
            or die("Не могу разблокировать SIGINT для fork: $!\n");

        $children{$pid} = 1;
        $children++;

        return;
    } else {
        # Часть программы для потомка, из которой он не может выйти.
        $SIG{INT} = 'DEFAULT';

        sigprocmask(SIG_UNBLOCK, $sigset)
            or die("Не могу разблокировать SIGINT для fork: $!\n");

        # Обработка подключений происходит тут
        for ($i=0; $i < $CLIENTS_PER_FORK; $i++) {
            handleConnection($$, $server->accept()) or last;
        }
            
        # Важно чтобы завершение сервера происходило именно так, иначе он плодит ещё 
        # больше и больше потомков
        exit;
    }
}

sub handleConnection{
    my ($pid, $client) = @_;


    printf "[Соединение из %s, обслуживает процесс %d]\n", $client->peerhost, $pid;

    my $clientMessage = "Добро пожаловать; введите \"help\" для списка комманд.
    Вас обслуживает процесс $pid\n";
    sendCMD($client,"msg", $clientMessage);

    while ($cmd = <$client>) {
    	
    	($cmd, $msg) = getCMD($cmd);
    	

    	$cmd0 = $cmd;
    	$cmd = convertComand($cmd);
    	$_ = $cmd;

    	if ($cmd == 0) {
    		sendCMD($client,"msg", getHelp());
    	}
    	elsif ($cmd == 1) {
    		sendCMD($client,"msg", getInfo($client));
    	}
    	elsif ($cmd == 2) {
    		sendCMD($client,"msg", getDate());
    	}
    	elsif ($cmd == 3) {
    		sendCMD($client,"msg", getTime());
    	}
    	elsif ($cmd == 4) { # load from client to server
    		loadFile($client,"");
    	}
    	elsif ($cmd == 5) { # load from client to server convert to ROME
    		loadFile($client,"toRome");
    	}
    	elsif ($cmd == 6) { # load from client to server convert to ARAB
    		loadFile($client,"toArab");
    	}
    	elsif ($cmd == 7) { # download from server to client
    		download($client);
    	}
    	else {
    		sendCMD($client,"msg", "Некорректная команда $cmd0\n");
    	}

    }

    close $client;
    printf "[Соединение закрыто: %s]\n", $client->peerhost;
}   

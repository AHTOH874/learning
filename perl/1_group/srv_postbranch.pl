#!/usr/bin/perl -w
use IO::Socket;
use Net::hostent;
use Sys::Hostname;
use POSIX qw(:sys_wait_h);

use lib '/home/user/Рабочий стол/9/new';

use protocol;

$server = IO::Socket::INET->new(Proto => 'tcp',
    LocalPort => 8081,
    Listen => SOMAXCONN,
    Proto => 'tcp',
    Reuse => 1
);

die "Не могу запустить сервер" unless $server;
print "[Сервер из файла $0 запущен]\n";

# REAPER чистит "мёртвых" потомков
sub REAPER {
    1 until (-1 == waitpid(-1, WNOHANG));
    $SIG{CHLD} = \&REAPER; 
}

# Установить обработчики сигналов
$SIG{CHLD} = \&REAPER;

while ($client = $server->accept()) {
    next if $pid = fork;
    die "fork: $!" unless defined $pid;
    $server->close();

    $pid = $$; # Устанавливаем pid для вывода пользователю.

    printf "[Соединение из %s, обслуживает процесс %d]\n", $client->peerhost, $pid;

    my $clientMessage = "Добро пожаловать; введите \"help\" для списка комманд.
    Вас обслуживает процесс $pid\n";
	sendCMD($client,"msg", $clientMessage);
	
    while ($cmd = <$client>) {
    	
    	($cmd, $msg) = getCMD($cmd);
    	

    	$cmd0 = $cmd;
    	$cmd = convertComand($cmd);
    	$_ = $cmd;

    	if ($cmd == 0) {
    		sendCMD($client,"msg", getHelp());
    	}
    	elsif ($cmd == 1) {
    		sendCMD($client,"msg", getInfo($client));
    	}
    	elsif ($cmd == 2) {
    		sendCMD($client,"msg", getDate());
    	}
    	elsif ($cmd == 3) {
    		sendCMD($client,"msg", getTime());
    	}
    	elsif ($cmd == 4) { # load from client to server
    		loadFile($client,"");
    	}
    	elsif ($cmd == 5) { # load from client to server convert to ROME
    		loadFile($client,"toRome");
    	}
    	elsif ($cmd == 6) { # load from client to server convert to ARAB
    		loadFile($client,"toArab");
    	}
    	elsif ($cmd == 7) { # download from server to client
    		download($client);
    	}
    	else {
    		sendCMD($client,"msg", "Некорректная команда $cmd0\n");
    	}

    }

    close $client;
    printf "[Соединение закрыто: %s]\n", $client->peerhost;
    exit; 
} continue {
    close $client;
}

#!/usr/bin/perl -w

use IO::Socket;
use Socket;
use Sys::Hostname;

$nums = [
	["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
	["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],
	["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"],
	["", "M", "MM", "MMM"]
	];

%nums = (
	"I" => 1,
	"V" => 5,
	"X" => 10,
	"L" => 50,
	"C" => 100,
	"D" => 500,
	"M" => 1000
	);

# функция чтения сообщения
sub readmsg($){
	my $client = shift;
	$_ = <$client>;
	m/command=(\d+)\s+length=(\d+)\n(.*)/s;
	my $command = $1;
	my $length = $2;
	my $msg = $3;
	my $d = length $msg;
	while($d < $length){
		my $m = <$client>;
		$msg .= $m;
		$d = length $msg;
	}
	chomp($msg);
	return ($command, $msg);
}

# функция отправки сообщения
sub sendmsg($$$){
	my ($client, $command, $msg) = @_;
	$msg .= "\n";
	my $length = length $msg;
	print $client "command=$command length=$length\n$msg";
}

# функция для перевода из арабской в римскую
sub a2r($){
	my $a = shift;
	my $r="";
	my $i = 0;
	$r = $nums->[$i++][($a % 10, $a = int($a / 10))[0] ].$r for 0..3;
	return $r;
}

# функция для перевода из римской в арабскую
sub r2a($){
	my $r = shift;
	my $l = length($r);
	my $a = $nums{substr($r, $l - 1, 1)};
	my $pred = $a;
	my $num;
	$a += (($num = $nums{substr($r, $_, 1)}) * (($num < $pred)? -1 : 1), $pred = $num)[0] for reverse 0..($l - 2);
	return $a;
}

# перехват сигнала Ctrl+C для закрытия сервера
$SIG{INT} = sub{
	print "\nЗавершение работы сервера\n";
	close($server);
	exit;
};

# определяем значение порта и создаем сокет
$port = 8080;

$server = IO::Socket::INET->new(Type => SOCK_STREAM,
				LocalPort => $port,
				Listen => SOMAXCONN,
				Reuse => 1) or die "$!\n";
				
print "Сервер работает\n";

# тут сервер ожидает новых клиентов и выполняет действия (читает сообщения от клиента, выполняет команду, отправляет результат клиенту)
NEWCLIENT:
while($client = $server->accept()){
	$client->autoflush(1);
	while(1){
		($command, $msg) = readmsg $client;
		if ($command == 1)
		{
			print "Подключился клиент\n";
			sendmsg($client, 2, "Сервер подтвердил подключение");
		}
		elsif ($command == 3)
		{
			print "Отключился клиент\n";
			sendmsg($client, 4, "\nСервер подтвердил отключение");
			close($client);
			next NEWCLIENT;
		}
		elsif ($command == 5)
		{
			print "Подтверждение от клиента: $msg\n";
		}
		elsif ($command == 6)
		{
			print "Клиент запросил данные об адресах\n";
			$client_ip = $client->peerhost;
			$server_ip = $client->sockhost();
			sendmsg($client, 7, "client $client_ip\nserver $server_ip");
		}
		elsif ($command == 8)
		{
			print "Клиент запросил дату и время\n";
			($sec, $min, $h, $d, $m, $y) = localtime;
			$y += 1900;
			$m++;
			sendmsg($client, 9, "\[$d.$m.$y $h:$min:$sec\]");
		}
		elsif ($command == 10)
		{
			print "Клиент передает файл без изменений\n";
			$path = $msg;
			$ss = 0;
		}
		elsif ($command == 11)
		{
			print "Клиент передает файл с переводом из арабской в римскую\n";
			$path = $msg;
			$ss = 1;
		}
		elsif ($command == 12)
		{
			print "Клиент передает файл с переводом из римской в арабскую\n";
			$path = $msg;
			$ss = 2;
		}
		elsif ($command == 13)
		{
			print "Прием файла от клиента\n";
			$msg =~s/(\b\d+\b)/a2r($1)/ge if($ss == 1);
			$msg =~s/\b((?i)M{0,3}(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(I[VX]|V?I{0,3}))\b/r2a($1)/ge if($ss == 2);
			print $1;
			open F, ">$path" or die "Ошибка открытия файла!: $!\n";
			print F $msg;
			close F;
			sendmsg($client, 14, "Файл принят");
		}
		elsif ($command == 15)
		{
			print "Передача файла $msg клиенту\n";
			open FILE, "<$msg" or die "Ошибка открытия файла!: $!\n";
			$msg = "";
			$msg .=$_ while(<FILE>);
			close FILE;
			sendmsg($client, 16, $msg);
		}
		elsif ($command == 16)
		{
			print "Получаю файлы в каталоге";
			my $files_list = `ls`;
			chomp $files_list;
			sendmsg($client, 1, $files_list)
		} else {
			print "Неизвестная команда\n";
		}	
	}
}
	


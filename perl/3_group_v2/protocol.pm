#!/usr/bin/perl

package protocol; 

use Exporter;

our @ISA = qw(Exporter);
our @EXPORT = qw(getCMD sendCMD convertComand getHelp getInfo getDate getTime getLs loadFile download toRome replaceNL);

use IO::Socket;
use Net::hostent;
use Sys::Hostname;

sub replaceNL {
	my ($text, $how) = @_;
    
    if ($how) {
        $text =~ s/\n/_CRLF_/ig;
        return $text;
    }
    
    $text =~ s/_CRLF_/\n/ig;
    return $text;
}

sub getCMD {
	my ($text) = @_;
	$_ = $text;
    m/cmd=(.*)\.msg=(.*)/s;
    my $cmd = $1;
	my $msg = $2;
	
	return ($cmd, $msg);
}

sub sendCMD {
	my ($socket, $cmd, $msg) = @_;
	chomp($cmd);
	chomp($msg);
	print $socket "cmd=$cmd.msg=$msg\n";
}

sub convertComand {
	my ($cmd) = @_;
	$_ = $cmd;
	chomp($cmd);
	if ($cmd eq "help") {return 0;}
	elsif ($cmd eq "info") {return 1;}
	elsif ($cmd eq "date") {return 2;}
	elsif ($cmd eq "time") {return 3;}
	elsif ($cmd eq "load") {return 4;}
	elsif ($cmd eq "load_r") {return 5;}
	elsif ($cmd eq "load_a") {return 6;}
	elsif ($cmd eq "download") {return 7;}
	elsif ($cmd eq "ls") {return 8;}
	else {return -1;}
	
}

sub getHelp {
	$text = "\n\tДоступные комманды:".
			"\n\tinfo \t - получить информацию о соединении".
			"\n\tdate \t - получить текущую дату".
			"\n\ttime \t - получить текущее время".
			"\n\tload \t - загрузка файла на сервер".
			"\n\tload_r \t - загрузка файла на сервер с переводом арабских цифр на римские".
			"\n\tload_a \t - загрузка файла на сервер с переводом римских цифр на арабские".
			"\n\tls \t - файлы в каталоге".
			"\n\tdownload - скачивание файла с сервера\n";
    
    return $text;
} 

sub getInfo {
	my ($client) = @_;
	$serverInfo = hostname();
    $clientInfo = gethostbyaddr($client->peeraddr);
    return "\n\tИнформация о соединении:\n\t" .
                           "Сервер: [$$]" . $serverInfo . "\n\t" . 
                            "Клиент: " . $clientInfo->name . "\n";
} 

sub getDate {
	($sec, $min, $h, $d, $m, $y) = localtime;
	$y += 1900;
	$m++;
    return "$d.$m.$y\n";
}  

sub getTime {
	($sec, $min, $h) = localtime;
    return "$h:$min:$sec\n";
} 

sub getLs {

	my $files_list = `ls`;
    chomp $files_list;

	return "$files_list\n";
}


sub loadFile {
	my ($client,$convert) = @_;
	
	sendCMD($client,"loadFile", "Введите путь к файлу на клиенте: ");

	chomp($clientPath = <$client>);
	($cmd, $msg) = getCMD($clientPath);
	$content = $msg;

	
	sendCMD($client,"msg", "Введите путь к файлу на сервере: ");

	chomp($answer = <$client>);
	($cmd, $msg) = getCMD($answer);
	$serverPath = $cmd;
	
	$content = replaceNL($content,0);
	
	if ($convert eq "toRome") {
		$content =~s/(\b\d+\b)/toRome($1)/ge;
	}
	elsif ($convert eq "toArab") {
		$content =~s/(\b(?=[MDCLXVI])M*(?:C[MD]|D?C{0,3})(?:X[CL]|L?X{0,3})(?:I[XV]|V?I{0,3})\b)/toArab($1)/ge;
	}
	
	open FILE, ">info";
	print FILE $serverPath;
	print FILE $content;
	close FILE;
	
	
	open FILE, ">/home/user/Рабочий стол/9/t2";
	print FILE $content;
	close FILE;
	
	sendCMD($client,"msg", "\n\tПередача файла с клиента на сервер выполнена");
} 

sub download {
	my ($client) = @_;
	
	sendCMD($client,"msg", "Введите путь к файлу на сервере: ");

	chomp($answer = <$client>);
	($cmd, $msg) = getCMD($answer);
	$serverPath = $cmd;
	
	
	$content = "";
	open FILE, "<$serverPath" or (warn "$!\n");
    $content .=$_ while(<FILE>);
    $content = replaceNL($content,1);
    close FILE;
	$content = $content."\n";
	
	sendCMD($client,"download", $content);
	
	chomp($answer = <$client>);
	
	sendCMD($client,"msg", "\n\tПередача файла с сервера на клиент выполнена");
	
} 

#-------------------------------------------------------------------------------

sub test{
	my ($num, $hi, $re, $lo) = @_;
	if ($num == 9) {
		return $lo.$hi;
	} elsif ($num == 8) {
		return $re.$lo.$lo.$lo;
	} elsif ($num == 7) {
		return $re.$lo.$lo;
	} elsif ($num == 6) {
		return $re.$lo;
	} elsif ($num == 5) {
		return $re;
	} elsif ($num == 4) {
		return $lo.$re;
	} elsif ($num == 3) {
		return $lo.$lo.$lo;
	} elsif ($num == 2) {
		return $lo.$lo;
	} elsif ($num == 1) {
		return $lo;
	} elsif ($num == 0) {
		return "";
	}
}

sub toRome {
	my ($num) = @_;
	$rome_num = "";
	$rome_num .= test(int($num/1000), "", "", "M");
	$num %= 1000;
	$rome_num .= test(int($num/100), "M", "D", "C");
	$num %= 100;
	$rome_num .= test(int($num/10), "C", "L", "X");
	$num %= 10;
	$rome_num .= test($num, "X", "V", "I");
	return $rome_num;
}

sub toArab {
	my ($num) = @_;
	my $totalValue = 0;
	my $value = 0;
	my $prev = 0;
	my @symbols = split(//, $num);
	for (my $i = 0; $i < length($num); $i++) {
		my $current = char_to_int(@symbols[$i]);
		if ($current > $prev) {
			$totalValue -= 2 * $value;
		}
		if ($current != $prev) {
			$value = 0;
		}
		$value += $current;
		$totalValue += $current;
		$prev = $current;
	}
	return $totalValue;
}

sub char_to_int {
	my ($c) = @_;
	if ($c eq "I") {
		return 1;
	} elsif ($c eq "V") {
		return 5;
	} elsif ($c eq "X") {
		return 10;
	} elsif ($c eq "L") {
		return 50;
	} elsif ($c eq "C") {
		return 100;
	} elsif ($c eq "D") {
		return 500;
	} elsif ($c eq "M") {
		return 1000;
	}
}
1;




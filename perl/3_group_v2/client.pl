#!/usr/bin/perl

use IO::Socket;

# функция чтения сообщения
sub readmsg($){
	my $client = shift;
	$_ = <$client>;
	m/command=(\d+)\s+length=(\d+)\n(.*)/s;
	my $command = $1;
	my $length = $2;
	my $msg = $3;
	my $d = length $msg;
	while($d < $length){
		my $m = <$client>;
		$msg .= $m;
		$d = length $msg;
	}
	chomp($msg);
	return ($command, $msg);
}

# функция отправки сообщения
sub sendmsg($$$){
	my ($client, $command, $msg) = @_;
	$msg .= "\n";
	my $length = length $msg;
	print $client "command=$command length=$length\n$msg";
}

# перехват сигнала Ctrl+C для закрытия клиента как нам надо: отправляем севреру сообщение о закрытии, читаем сообщение от него, закрываем клиента и завершаем программу
$SIG{INT}= sub{
	sendmsg($client, 3, "");
	($command, $msg) = readmsg($client);
	print "$msg\n";
	$client->close;
	print "\n";
	exit;
};

# адрес и порт клиента
$host = 'localhost';	
$port = 8080;

# создание сокета клиента
$client = IO::Socket::INET->new(
		PeerAddr => $host,
		PeerPort => $port,
		Proto => 'tcp',
		Type => SOCK_STREAM)
	or die "$!\n";

# очистка буфера
$client->autoflush(1);

# отправка сообщения серверу и чтнеие сообщения от сервера
sendmsg($client, 1, "");
($command, $msg) = readmsg($client);
print "$msg\n";

print "\nДоступные команды:\n";
print "host - адреса клиента и сервера\n";
print "time - узнать текущее время\n";
print "send - отправить файл без изменений\n";
print "send ar - отправить файл с переводом чисел из арабской в римскую систему\n";
print "send ra - отправить файл с переводом чисел из римской в арабскую систему\n";
print "download - скачать файл с сервера\n";
print "ls - файлы в каталоге\n";
print "<Ctrl>+<C> - отключиться\n";

# здесь выполняется определение ввода команды и выполнение команд со стороны клиента (отправка сообщений серверу и чтение сообщений от сервера)
for(;;){
	print "\nВведите команду: ";
	
	chomp($command = <STDIN>);
	print "\n";
	if ($command eq 'host') {
		sendmsg($client, 6, "");
		($command, $_) = readmsg($client);
		m/client (.+)\nserver (.+)/s;
		print "Клиент: $1\nСервер: $2\n";
		sendmsg($client, 5, "адреса узнал");
	} elsif ($command eq 'time') {
		sendmsg($client, 8, "");
		($command, $_) = readmsg($client);
		m/(\d+)\.(\d+)\.(\d+) (\d+):(\d+):(\d+)/s;
		print "Текущие дата и время:\nДата: $1/$2/$3\nВремя: $4:$5:$6\n";
		sendmsg($client, 5, "время узнал");
	} elsif ($command eq 'send') {
		print "Отправка файла серверу\nВведите путь к файлу на клиенте: ";
		chomp($file = <STDIN>);
		print "Введите путь к файлу на сервере: ";
		chomp($serv_file = <STDIN>);
		sendmsg($client, 10, $serv_file);
		open FILE, "<$file" or (warn "$!\n", next);
		$msg = "";
		$msg .=$_ while(<FILE>);
		close FILE;
		sendmsg($client, 13, $msg);
		($command, $msg) = readmsg($client);
		print "Ответ от сервера: $msg\n";
	} elsif ($command eq 'send ar') {
		print "Отправка файла серверу\nВведите путь к файлу на клиенте: ";
		chomp($file = <STDIN>);
		print "Введите путь к файлу на сервере: ";
		chomp($serv_file = <STDIN>);
		sendmsg($client, 11, $serv_file);
		open FILE, "<$file" or (warn "$!\n", next);
		$msg = "";
		$msg .=$_ while(<FILE>);
		close FILE;
		sendmsg($client, 13, $msg);
		($command, $msg) = readmsg($client);
		print "Ответ от сервера: $msg\n";
	} elsif ($command eq 'send ra') {
		print "Отправка файла серверу\nВведите путь к файлу на клиенте: ";
		chomp($file = <STDIN>);
		print "Введите путь к файлу на сервере: ";
		chomp($serv_file = <STDIN>);
		sendmsg($client, 12, $serv_file);
		open FILE, "<$file" or (warn "$!\n", next);
		$msg = "";
		$msg .=$_ while(<FILE>);
		close FILE;
		sendmsg($client, 13, $msg);
		($command, $msg) = readmsg($client);
		print "Ответ от сервера: $msg\n";
	} elsif ($command eq 'download') {
		print "Прием файла с сервера\nВведите путь к файлу на сервере: ";
		chomp($serv_file = <STDIN>);
		print "Введите путь к файлу на клиенте: ";
		chomp($file = <STDIN>);
		sendmsg($client, 15, $serv_file);
		($command, $msg) = readmsg($client);
		open FILE, ">$file";
		print FILE $msg;
		close FILE;
		sendmsg($client, 5, "Клиент: файл принят");
	} elsif ($command eq 'ls') {
		sendmsg($client, 16, "");
		($command, $msg) = readmsg($client);
		print "ls: $msg\n";
		sendmsg($client, 5, "Прочитан каталог клиентом");

	} else {
		print "Неизвестная команда\n";
	}
}

#!/usr/bin/perl

use IO::Socket;
use Socket;
use Sys::Hostname;
use POSIX;

$PID_FILE = "/home/kristina/university/networks/lab1/Lab1/PID_FILE.pid";
$LOG = "/home/kristina/university/networks/lab1/Lab1/LOGFILE.log";
$port = 8080;

# тут осущетсвялется запись времени и некоторого сообщения (действия сервера) в лог файл
sub Log($){
	my $msg = shift;
	my ($sec, $min, $h, $d, $m, $y) = localtime;
	$y += 1900;
	$m++;
	print LOG "\[$d.$m.$y $h:$min:$sec\] $msg\n";
}

# перехват ктрл ц и завершение работы сервера
$SIG{INT} = sub {
	Log "Сервер остановлен";
	close LOG;
	exit(EXIT_SUCCESS);
};

sub readmsg($){
	my $client = shift;
	$_ = <$client>;
	m/command=(\d+)\s+length=(\d+)\n(.*)/s;
	my $command = $1;
	my $length = $2;
	my $msg = $3;
	my $d = length $msg;
	while($d < $length){
		my $m = <$client>;
		$msg .= $m;
		$d = length $msg;
	}
	chomp($msg);
	return ($command, $msg);
}

sub sendmsg($$$){
	my ($client, $command, $msg) = @_;
	$msg .= "\n";
	my $length = length $msg;
	print $client "command=$command length=$length\n$msg";
}

sub a2r($){
	my $a = shift;
	my $r="";
	my $i = 0;
	$r = $nums->[$i++][($a % 10, $a = int($a / 10))[0] ].$r for 0..3;
	return $r;
}

sub r2a($){
	my $r = shift;
	my $l = length($r);
	my $a = $nums{substr($r, $l - 1, 1)};
	my $pred = $a;
	my $num;
	$a += (($num = $nums{substr($r, $_, 1)}) * (($num < $pred)? -1 : 1), $pred = $num)[0] for reverse
	0..($l - 2);
	return $a;
}

#тут вся работа по выполнению команд и общению с клиентом
sub client{
	my $client = shift;
	$client->autoflush(1);
	while(1){
		($command, $msg) = readmsg($client);
		if ($command == 1) {
			Log "Подключился клиент";
			sendmsg($client, 2, "Сервер подтвердил подключение");
		}
		elsif ($command == 3)
		{
			Log "Отключился клиент";
			sendmsg($client, 4, "\nСервер подтвердил отключение");
			close($client);
			exit;
		}
		elsif ($command == 5)
		{
			Log "Подтверждение от клиента: $msg";
		}
		elsif ($command == 6)
		{
			print "Клиент запросил данные об адресах\n";
			$client_ip = $client->peerhost;
			$server_ip = $client->sockhost();
			sendmsg($client, 7, "client $client_ip\nserver $server_ip");
		}
		elsif ($command == 8)
		{
			Log "Клиент запросил дату и время";
			my ($sec, $min, $h, $d, $m, $y)= localtime;
			$y += 1900;
			$m++;
			sendmsg($client, 9, "\[$d.$m.$y $h:$min:$sec\]");
		}
		elsif ($command == 10)
		{
			Log "Клиент передает файл без изменений";
			$path = $msg;
			$ss = 0;
		}
		elsif ($command == 11)
		{
			Log "Клиент передает файл с переводом из арабской в римскую";
			$path = $msg;
			$ss = 1;
		}
		elsif ($command == 12)
		{
			Log "Клиент передает файл с переводом из римской в арабскую";
			$path = $msg;
			$ss = 2;
		}
		elsif ($command == 13)
		{
			Log "Прием файла от клиента";
			$msg =~s/(\b\d+\b)/a2r($1)/ge if($ss == 1);
			$msg =~s/\b((?i)M{0,3}(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(I[VX]|V?I{0,3}))\b/r2a($1)/ge if($ss == 2);
			open FILE, ">$path";
			print FILE $msg;
			close FILE;
			sendmsg($client, 14, "Файл принят");
		}
		elsif ($command == 15)
		{
			Log "Передача файла $msg клиенту";
			open FILE, "<$msg";
			$msg = "";
			$msg .=$_ while(<FILE>);
			close FILE;
			sendmsg($client, 16, $msg);
		} else {
			Log "Неизвестная комманда";
		}
	}
};

# тут создается сервер демон
sub Daemon(){
	$server = IO::Socket::INET->new(
			LocalPort => $port,
			Type => SOCK_STREAM,
			Reuse => 1,
			Listen => SOMAXCONN) or (Log($!), exit);
	Log "Сервер запущен";
	for(;;){
		$client = $server->accept();
		client($client) if fork == 0;
	}
}

# тут запускается сервер демон
sub StartDaemon(){
	if (-e $PID_FILE){
		print "Сервер уже запущен\n";
		exit(EXIT_FAILURE)
	}
	
	open PID, ">$PID_FILE" or die "Невозможно создать файл. $!\n";
	$pid = fork;
	
	unless(defined $pid){
		print "Ошибка запуска\n";
		unlink "$PID_FILE";
		exit(EXIT_FAILURE)
	}
	
	if($pid > 0){	
		print PID $pid;
		print "Сервер запущен\n";
		open LOG, ">>", $LOG;
		close PID;
	}
	else{
		open LOG, ">>", $LOG;
		umask 0;
		POSIX::setsid;
		chdir "/";
		close STDIN;
		close STDOUT;
		close STDERR;
		Daemon;
	}
}

# тут останавливается сервер демон
sub StopDaemon(){
	unless(-e $PID_FILE){
		print "Сервер не запущен\n";
		exit(EXIT_FAILURE);
	}
	
	open PID, "<$PID_FILE";
	$pid = <PID>;
	close PID;
	kill 2, $pid;
	print "Сервер остановлен\n";
	unlink "$PID_FILE";
}

# проверка на наличие параметра(аргумента)
if(@ARGV == 0){
	print "Команда не введена\n";
	exit(EXIT_FAILURE);
}

# вызов той или иной функции в зависимости от введенной команды
if ($ARGV[0] eq "strt") {
	StartDaemon;
	exit(EXIT_SUCCESS);
} elsif ($ARGV[0] eq "stp") {
	StopDaemon;
	exit(EXIT_SUCCESS);
} elsif ($ARGV[0] eq "rstrt") {
	StopDaemon;
	StartDaemon;
	exit(EXIT_SUCCESS);
} else {
	print "Неизвестный параметр\n";
	exit(EXIT_FAILURE);
}


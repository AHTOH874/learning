#!/usr/bin/perl

use IO::Socket;
use Socket;
use Sys::Hostname;
use POSIX;

%servers = ();

$nums = [
	["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
	["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],
	["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"],
	["", "M", "MM", "MMM"]
	];

%nums = (
	"I" => 1,
	"V" => 5,
	"X" => 10,
	"L" => 50,
	"C" => 100,
	"D" => 500,
	"M" => 1000
	);


sub r2a($)
{
	my $rim = shift;
	my $len = length($rim);
	my $ar = $nums{substr($rim, $len - 1, 1)};
	my $pred = $ar;
	my $num;
	$ar += (($num = $nums{substr($rim, $_, 1)}) * (($num < $pred)? -1 : 1), $pred = $num)[0] for reverse 0..($len - 2);
	return $ar;
}

sub a2r($)
{
	my $ar = shift;
	my $rim ="";
	my $i = 0;
	$rim = $nums->[$i++][($ar % 10, $ar = int($ar / 10))[0] ].$rim for 0..3;
	return $rim;
}

sub readmsg($)
{
	my $client = shift;
	$_ = <$client>;
	m/command=(\d+)\s+length=(\d+)\n(.*)/s;
	my $command = $1;
	my $length = $2;
	my $msg = $3;
	my $d = length $msg;
	while($d < $length)
	{
		my $m = <$client>;
		$msg .= $m;
		$d = length $msg;
	}
	chomp($msg);
	return ($command, $msg);
}


sub sendmsg($$$)
{
	my ($client, $command, $msg) = @_;
	$msg .= "\n";
	my $length = length $msg;
	print $client "command=$command length=$length\n$msg";
}

$SIG{INT} = sub
{
	close($serv);
	close($s1);
	close($s2);
	close($s3);
	print "\nСервер завершает свою работу\n";
	exit;
};


sub server
{
	$sock = shift;
	
	$serv = IO::Socket::INET->new(
			LocalPort => $port + $sock,
			Type => SOCK_STREAM,
			Reuse => 1,
			Listen => SOMAXCONN) or die "$!\n";
	sleep 1;

	if($sock == 0)
	{
		$s1 = IO::Socket::INET->new(
			PeerAddr => 'localhost',
			PeerPort => $port + 1,
			Proto => 'tcp',
			Type => SOCK_STREAM) or die "$!\n";
		
		$s2 = IO::Socket::INET->new(
			PeerAddr => 'localhost',
			PeerPort => $port + 2,
			Proto => 'tcp',
			Type => SOCK_STREAM) or die "$!\n";
		
		$se3 = IO::Socket::INET->new(
			PeerAddr => 'localhost',
			PeerPort => $port + 3,
			Proto => 'tcp',
			Type => SOCK_STREAM) or die "$!\n";
	}

	NEWCLIENT:

	while($client = $serv->accept())
	{
		$client->autoflush(1);
		if($sock == 0)
		{	
			while(1)
			{
				($menu, $msg) = readmsg $client;
				if($menu == 1)
				{
					print "Подключился клиент\n";
					sendmsg($client, 2, "Сервер подтвердил подключение");
				}
				elsif($menu == 3)
				{
					print "Клиент отключился\n";
					sendmsg($client, 4, "\nСервер подтвердил отключение");
					close($client);
					next NEWCLIENT;
				}
				elsif($menu == 5)
				{
					print "Подтверждение от клиента: $msg\n";
				}
				elsif($menu == 6)
				{
					print "Клиент запросил данные об адресах\n";
					$client_ip = $client->peerhost;
					$server_ip = $client->sockhost();
					sendmsg($client, 7, "client $client_ip\nserver $server_ip");
				}	
				elsif($menu == 8)
				{
					print "Перенаправление на сервер 1\n";
					print "Клиент запросил дату и время\n";
					sendmsg($s1, 8, "");
					($menu, $msg) = readmsg($s1);
					sendmsg($client, 9, $msg);
				}
				elsif($menu == 10 or $menu == 11 or $menu == 12)
				{
					print "Перенаправление на сервер 2\n";
					sendmsg($s2, $menu, $msg);
				}
				elsif($menu == 13)
				{
					sendmsg($s2, 13, $msg);
					($menu, $msg) = readmsg($s2);
					sendmsg($client, $menu, $msg);
				}
				elsif($menu == 15)
				{
					print "Перенаправление на сервер 3\n";
					sendmsg($s3, 15, $msg);
					($menu, $msg) = readmsg($s3);
					sendmsg($client, $menu, $msg);
				}
			}
		}
		elsif($sock == 1)
		{
			while(1)
			{
				($menu, $msg) = readmsg $client;
				($sec, $min, $h, $d, $m, $y) = localtime;
				$y += 1900;
				$m++;
				sendmsg($client, 0, "\[$d.$m.$y $h:$min:$sec\]");
			}
		}
		elsif($sock == 2)
		{
			while(1)
			{
				($menu, $msg) = readmsg $client;
				if($menu == 10)
				{
					print "Клиент передает файл без изменений\n";
					$path = $msg;
					$buf = 0;
				}
				elsif($menu == 11)
				{
					print "Клиент передает файл с переводом из арабской в римскую\n";
					$path = $msg;
					$buf = 1;
				}
				elsif($menu == 12)
				{
					print "Клиент передает файл с переводом из римской в арабскую\n";
					$path = $msg;
					$buf = 2;
				}
				elsif($menu == 13)
				{
					$msg =~s/(\b\d+\b)/a2r($1)/ge if($buf == 1);
					$msg =~s/\b((?i)M{0,3}(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(I[VX]|V?I{0,3}))\b/r2a($1)/ge if($buf == 2);
					open FILE, ">$path";
					print FILE $msg;
					close FILE;
					sendmsg($client, 14, "файл принят!");
				}
			}

		}
		elsif($sock == 3)
		{
			while(1)
			{
				($menu, $msg) = readmsg $client;
				open FILE, "<$msg";
				$msg = "";
				$msg .=$_ while(<FILE>);
				close FILE;
				sendmsg($client, 16, $msg);
			}
		}
	}
}
$sock = 0;
$port = 8080;

for($i = 1; $i < 4; $i++)
{
	$pid = fork;
	
	if($pid == 0)
	{
		server($i);
	}
	else
	{
		$servers{$pid} = 1;
	}
}
server(0); 

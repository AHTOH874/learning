#!/usr/bin/perl

use utf8;
use strict;
use warnings;
use IO::File;
use IO::Socket::INET;

# Define the server address and port
my $server_address = "localhost";
my $server_port = 35847;

# Create a new socket and bind it to the server address and port
my $socket = IO::Socket::INET->new(
    LocalAddr => $server_address,
    LocalPort => $server_port,
    Proto => 'tcp',
    Listen => 1
) or die "Could not create socket: $!\n";

print "Server start listening\n";

# Wait for incoming connections
while (my $client_socket = $socket->accept) {
    # Read the command from the client socket
    my $cmd = "";
    $client_socket->recv($cmd, 1024);
    chomp $cmd;

    if ($cmd eq "datetime") {
        # Passing pointer to client socket
        datetime(\$client_socket);
    } elsif ($cmd eq 'ls') {
        # Passing pointer to client socket
        ls(\$client_socket);
    } elsif ($cmd eq "r2a") {
        # Passing pointer to client socket
        r2a(\$client_socket);
    } elsif ($cmd eq "a2r") {
        # Passing pointer to client socket
        a2r(\$client_socket);
    } elsif ($cmd eq "retr") {
        # Passing pointer to client socket
        retr(\$client_socket);
    } elsif ($cmd eq "stor") {
        # Passing pointer to client socket
        stor(\$client_socket);
    } elsif ($cmd eq "exit") {
        # Close the client socket
        $client_socket->close();
        last;
    }

    # Close the client socket
    $client_socket->close();
}

# Close the server socket
$socket->close();

print "Server stop listening\n";

# Now datetime
sub datetime {
    my $client_socket = shift;
    
    # Dereferencing the pointer
    $client_socket = $$client_socket;

    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = 
localtime(time);
    my $datetime = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $year + 1900, $mon + 1, 
$mday, $hour, $min, $sec);

    print $client_socket "$datetime\n";
}

# Files list
sub ls {    
    my $client_socket = shift;
    
    # Dereferencing the pointer
    $client_socket = $$client_socket;

    my $files_list = `ls`;
    chomp $files_list;

    print $client_socket "$files_list\n";
}

# Convert Roman numerals to Arabic numerals from file and send to client 
sub r2a {
    my $client_socket = shift;
    
    # Dereferencing the pointer
    $client_socket = $$client_socket;

    # Read the path to file of the client socket
    my $path = "roman_numbers.txt";
    # $client_socket->recv($path, 1024);
    # chomp $path;

    # Check existing file
    unless (-e $path) {
        print $client_socket "$path not exists\n";
        return;
    }

    # Check file size
    my $file_size = -s $path;
    if ($file_size > 1024) {
        print $client_socket "$path too big. Max file size 1KB\n";
        return;
    }

    # Inform client that there no errors
    print $client_socket "\n";

    # Open the file to be sent
    # my $fh = IO::File->new($path, "r") or die "Could not open file: $!\n";
    open(my $fh, "<", "$path") or die "Could not open file: $!\n";

    # Read the contents of the file into a variable
    my $data = do { local $/; <$fh> };

    my @data_lines = split /\n/, $data;

    # Convert Roman numerals to Arabic numerals
    my $converted_data = "";
    foreach my $line (@data_lines) {
        chomp $line;
        my $converted = roman_to_arabic($line);
        $converted_data .= "$converted\n";
    }
    chomp $converted_data;

    print $client_socket $converted_data;

    # Close the file
    $fh->close();
}

# Converts Roman numerals to Arabic numerals
# Max number 3999
sub roman_to_arabic {
    my $input = shift;
    my %romans = (
        I => 1,
        IV => 4,
        V => 5,
        IX => 9,
        X => 10,
        XL => 40,
        L => 50,
        XC => 90,
        C => 100,
        CD => 400,
        D => 500,
        CM => 900,
        M => 1000
    );
    my $result = 0;
    while ($input) {
        foreach my $roman (keys %romans) {
            if ($input =~ /^$roman/) {
                $result += $romans{$roman};
                $input =~ s/^$roman//;
                last;
            }
        }
    }
    return $result;
}

# Convert Arabic numerals to Roman numerals from file and send to client 
sub a2r {
    my $client_socket = shift;
    
    # Dereferencing the pointer
    $client_socket = $$client_socket;

    # Read the path to file of the client socket
    my $path = "arabic_numbers.txt";
    # $client_socket->recv($path, 1024);
    # chomp $path;

    # Check existing file
    unless (-e $path) {
        print $client_socket "$path not exists\n";
        return;
    }

    # Check file size
    my $file_size = -s $path;
    if ($file_size > 1024) {
        print $client_socket "$path too big. Max file size 1KB\n";
        return;
    }

    # Inform client that there no errors
    print $client_socket "\n";

    # Open the file to be sent
    # my $fh = IO::File->new($path, "r") or die "Could not open file: $!\n";
    open(my $fh, "<", "$path") or die "Could not open file: $!\n";

    # Read the contents of the file into a variable
    my $data = do { local $/; <$fh> };

    my @data_lines = split /\n/, $data;

    # Convert Arabic numerals to Roman numerals
    my $converted_data = "";
    foreach my $line (@data_lines) {
        chomp $line;
        my $converted = arabic_to_roman($line);
        $converted_data .= "$converted\n";
    }
    chomp $converted_data;

    print $client_socket $converted_data;

    # Close the file
    $fh->close();
}

# Converts Arabic numerals to Roman numerals
# Max number 3999
sub arabic_to_roman {
    my $input = shift;
    my %arabic = (
        1000 => 'M',
        900 => 'CM',
        500 => 'D',
        400 => 'CD',
        100 => 'C',
        90 => 'XC',
        50 => 'L',
        40 => 'XL',
        10 => 'X',
        9 => 'IX',
        5 => 'V',
        4 => 'IV',
        1 => 'I'
    );
    my $result = "";
    foreach my $arabic (sort { $b <=> $a } keys %arabic) {
        while ($input >= $arabic) {
            $result .= $arabic{$arabic};
            $input -= $arabic;
        }
    }
    return $result;
}

# Send file to client
sub retr {
    my $client_socket = shift;
    
    # Dereferencing the pointer
    $client_socket = $$client_socket;

    # Read the path to file of the client socket
    my $path = "server_file.txt";
    # $client_socket->recv($path, 1024);
    # chomp $path;

    # Check existing file
    unless (-e $path) {
        print $client_socket "$path not exists\n";
        return;
    }

    # Check file size
    my $file_size = -s $path;
    if ($file_size > 1024) {
        print $client_socket "$path too big. Max file size 1KB\n";
        return;
    }

    # Inform client that there no errors
    print $client_socket "\n";

    # Open the file to be sent
    # my $fh = IO::File->new($path, "r") or die "Could not open file: $!\n";
    open(my $fh, "<", "$path") or die "Could not open file: $!\n";

    # Read the contents of the file into a variable
    my $data = do { local $/; <$fh> };

    print $client_socket $data;

    # Close the file
    $fh->close();
}

# Receive file from client
sub stor {
    my $client_socket = shift;

    # Dereferencing the pointer
    $client_socket = $$client_socket;

    # Read the filename from the client socket
    # my $filename = '';
    # $client_socket->recv($filename, 1024);
    # chomp $filename;

    # Open a file to write the receoved data to
    # my $fh = IO::File->new(">$filename") or die "Could not open file: $!\n";
    open(my $fh, ">", "client_file.txt") or die "Could not open file: $!";


    # Read the contents of the socket into a variable
    my $data = "";
    $client_socket->recv($data, 1024);

    # Write the contents of the socket to the file
    print $fh $data;

    # Close the file
    $fh->close();
}
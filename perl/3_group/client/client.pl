#!/usr/bin/perl

use utf8;
use strict;
use warnings;
use File::Basename;
use IO::File;
use IO::Socket::INET;

# Define the server address and port
my $server_address = "localhost";
my $server_port = 35847;

while (1) {
    # Create a new socket and connect to the server
    my $socket = IO::Socket::INET->new(
        PeerAddr => $server_address,
        PeerPort => $server_port,
        Proto => "tcp"
    ) or die "Could not create socket: $!\n";

    # Read the user's command-
    print "Enter command: ";
    my $cmd  = "";
    $cmd = <STDIN>;
    chomp $cmd;

    if ($cmd eq "datetime") {
        # Passing pointer to socket
        datetime(\$socket);
    } elsif ($cmd eq "ls") {
        # Passing pointer to socket
        ls(\$socket);
    } elsif ($cmd eq "r2a") {
        # Passing pointer to client socket
        r2a(\$socket);
    } elsif ($cmd eq "a2r") {
        # Passing pointer to client socket
        a2r(\$socket);
    } elsif ($cmd eq "retr"){
        # Passing pointer to socket
        retr(\$socket);
    } elsif ($cmd eq "stor") {
        # Passing pointer to socket
        stor(\$socket);
    } elsif ($cmd eq "exit") {
        ext(\$socket);
        # Close the socket
        $socket->close();
        last;
    }

    # Close the socket
    $socket->close();
}

# Now datetime on the server
sub datetime {
    my $socket = shift;

    # Dereferencing the pointer
    $socket = $$socket;

    print $socket "datetime\n";

    my $datetime = "";
    $socket->recv($datetime, 1024);
    chomp $datetime;

    print "$datetime\n";
}

# Files list on the server
sub ls {
    my $socket = shift;

    # Dereferencing the pointer
    $socket = $$socket;

    print $socket "ls\n";

    my $files_list = "";
    $socket->recv($files_list, 1024);
    chomp $files_list;

    print "$files_list\n";
}

# Receive the converted Roman numerals to Arabic numerals file on the server
sub r2a {
    my $socket = shift;

    # Dereferencing the pointer
    $socket = $$socket;

    # Read the path on server from the user
    # print "Enter the path to file for convert on server: ";
    my $path = "roman_numbers.txt";
    # $path = <STDIN>;
    # chomp $path;

    print $socket "r2a\n";
    # print $socket "$path\n";

    # Read error file on server
    my $error_file_on_server = '';
    $socket->recv($error_file_on_server, 1024);
    chomp $error_file_on_server;

    unless ($error_file_on_server eq '') {
        print "server return error: $error_file_on_server\n";
        return;
    }

    my $filename = basename($path);

    # Open a file to write the receoved data to
    my $fh = IO::File->new(">converted $filename") or die "Could not open file: 
$!\n";

    # Read the contents of the socket into a variable
    my $data = "";
    $socket->recv($data, 1024);

    # Write the contents of the socket to the file
    print $fh $data;

    # Close the file
    $fh->close();
}

# Receive the converted Arabic numerals to Roman numerals file on the server 
sub a2r {
    my $socket = shift;

    # Dereferencing the pointer
    $socket = $$socket;

    # Read the path on server from the user
    # print "Enter the path to file for convert on server: ";
    my $path = "arabic_number.txt";
    # $path = <STDIN>;
    # chomp $path;

    print $socket "a2r\n";
    # print $socket "$path\n";

    # Read error file on server
    my $error_file_on_server = '';
    $socket->recv($error_file_on_server, 1024);
    chomp $error_file_on_server;

    unless ($error_file_on_server eq '') {
        print "server return error: $error_file_on_server\n";
        return;
    }

    my $filename = basename($path);

    # Open a file to write the receoved data to
    my $fh = IO::File->new(">converted $filename") or die "Could not open file: 
$!\n";

    # Read the contents of the socket into a variable
    my $data = "";
    $socket->recv($data, 1024);

    # Write the contents of the socket to the file
    print $fh $data;

    # Close the file
    $fh->close();
}

# Receive the file from server
sub retr {
    my $socket = shift;

    # Dereferencing the pointer
    $socket = $$socket;

    # Read the path on server from the user
    # print "Enter the path to file on server: ";
    my $path = "server_file.txt";
    # $path = <STDIN>;
    # chomp $path;

    print $socket "retr\n";
    # print $socket "$path\n";

    # Read error file on server
    my $error_file_on_server = '';
    $socket->recv($error_file_on_server, 1024);
    chomp $error_file_on_server;

    unless ($error_file_on_server eq '') {
        print "server return error: $error_file_on_server\n";
        return;
    }

    my $filename = basename($path);

    # Open a file to write the receoved data to
    # my $fh = IO::File->new(">$filename") or die "Could not open file: $!\n";
    open(my $fh, ">", "$filename") or die "Could not open file: $!";

    # Read the contents of the socket into a variable
    my $data = "";
    $socket->recv($data, 1024);

    # Write the contents of the socket to the file
    print $fh $data;

    # Close the file
    $fh->close();
}

# Send the file to server
sub stor {
    my $socket = shift;
    
    # Dereferencing the pointer
    $socket = $$socket;

    # Read the path from the user
    # print "Enter the path to file: ";
    my $path = "client_file.txt";
    # $path = <STDIN>;
    # chomp $path;
    
    # Check existing file
    unless (-e $path) {
        print "$path not exists\n";
        return;
    }

    # Check file size
    my $file_size = -s $path;
    if ($file_size > 1024) {
        print "$path too big. Max file size 1KB\n";
        return;
    }

    # my $filename = basename($path);

    # Open the file to be sent
    # my $fh = IO::File->new($path, "r") or die "Could not open file: $!\n";
    open(my $fh, "<", "$path") or die "Could not open file: $!\n";

    # Read the contents of the file into a variable
    my $data = do { local $/; <$fh> };

    print $socket "stor\n";
    # print $socket "$filename\n";
    print $socket $data;

    # Close the file
    $fh->close();
}

sub ext {
    my $socket = shift;
    
    # Dereferencing the pointer
    $socket = $$socket;

    print $socket "exit\n";
}
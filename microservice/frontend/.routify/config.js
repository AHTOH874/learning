module.exports = {
  "pages": "src/pages",
  "sourceDir": "public",
  "routifyDir": ".routify",
  "ignore": "",
  "dynamicImports": true,
  "singleBuild": false,
  "noHashScroll": false,
  "distDir": "dist",
  "hashScroll": true,
  "extensions": [
    "html",
    "svelte",
    "md",
    "svx"
  ],
  "started": "2023-05-15T17:04:23.498Z"
}
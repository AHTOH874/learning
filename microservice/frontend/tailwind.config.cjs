module.exports = {
  darkMode: 'class',
  content: [
    "./index.html", './src/**/*.svelte', './src/**/*.css'
  ],
  theme: {
    extend: {},
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: ["pastel", "dark"],
  },
}

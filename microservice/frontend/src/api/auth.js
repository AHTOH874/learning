import { API_URL_AUTH_SERVICE, get, post } from "./api"

const BASE_URL = API_URL_AUTH_SERVICE + "/user"

const SIGN_UP_URL = BASE_URL + "/signup"
const LOGIN_URL = BASE_URL + "/login"

export async function login(params) {
    return post(LOGIN_URL, params)
}

export async function signup(params) {
    return post(SIGN_UP_URL, params)
}
import { API_URL_FILE_SERVICE, get, post_file } from "./api"

const GET_FILE_URL = API_URL_FILE_SERVICE + "/files/"
const GET_FILES_URL = API_URL_FILE_SERVICE + "/files"

const UPLOAD_FILE_URL = API_URL_FILE_SERVICE + "/upload_file"


export async function getFiles() {
    return get(GET_FILES_URL)
}

export async function getFile(filename) {
    return get(GET_FILE_URL + filename)
}

export async function uploadFile(file) {
    return post_file(UPLOAD_FILE_URL, file)
}
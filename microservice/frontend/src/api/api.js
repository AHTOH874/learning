import { get as get_store } from 'svelte/store'
import { jwt } from '../storable.js'


export const API_URL_AUTH_SERVICE = "http://localhost:8089/"
export const API_URL_FILE_SERVICE = "http://localhost:8081/"



export async function get(url) {
    url = url.replace(/([^:]\/)\/+/g, "$1")
    
    const response = await fetch(url, {
        headers: {
            'Authorization': 'Bearer ' + get_store(jwt),
        }
    })
    
    const contentType = response.headers.get("content-type");

    if (contentType && contentType.indexOf("application/json") !== -1) {
        return response.json()
    } else {
        return response
    }

}


export async function post(url, data) {
    url = url.replace(/([^:]\/)\/+/g, "$1")
    // Example POST method implementation:

    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + get_store(jwt),
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });

    return response.json(); // parses JSON response into native JavaScript objects
}

export async function post_file(url, data) {
    url = url.replace(/([^:]\/)\/+/g, "$1")

    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Authorization': 'Bearer ' + get_store(jwt),
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: data // body data type must match "Content-Type" header
    });

    return response.json(); // parses JSON response into native JavaScript objects
}
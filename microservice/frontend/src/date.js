export const dateFormat = { year: "numeric", month: "short", day: "numeric" }
export const dateFormatFull = { year: "numeric", month: "long", day: "numeric", hour12: true, hour: "2-digit", minute: "2-digit" }

export function parseDate(date) {
    return new Date(date).toLocaleDateString("ru-RU", dateFormat)
}

export function parseDateTime(date) {
    return new Date(date).toLocaleDateString("ru-RU", dateFormatFull)
}
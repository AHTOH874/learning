import { writable } from 'svelte/store'

// Get the value out of storage on load.
const stored = localStorage.content
// or localStorage.getItem('content')

// Set the stored value or a sane default.
export const jwt = writable(stored || '')

// Anytime the store changes, update the local storage value.
jwt.subscribe((value) => localStorage.content = value)
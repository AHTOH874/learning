from typing_extensions import Annotated

from fastapi import FastAPI, Body, Depends, UploadFile, File
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware

from minio import Minio
from minio.error import S3Error

from app.model import PostSchema
from app.auth.auth_bearer import JWTBearer
from app.auth.auth_handler import signJWT,decodeJWT


def create_app() -> FastAPI:

    client = Minio(
        "play.min.io",
        access_key="Q3AM3UQ867SPQQA43P2F",
        secret_key="zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
    )


    app = FastAPI()
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.get("/", tags=["root"])
    async def root_route() -> dict:
        return { "health": "ok"}


    @app.get("/files", dependencies=[Depends(JWTBearer())], tags=["files"])
    def get_files(token: dict = Depends(JWTBearer())) -> dict:

        payload = decodeJWT(token)
        user_id = payload["user_id"].split("@")[0]

        found = client.bucket_exists(user_id)
        if not found:
            client.make_bucket(user_id)
        else:
            print(f"Bucket {user_id} already exists")
        
        objects = client.list_objects(user_id)

        return objects

    @app.get("/files/{object_name}", dependencies=[Depends(JWTBearer())], tags=["files"], response_class=FileResponse)
    def get_file(object_name: str, token: dict = Depends(JWTBearer())) -> File:

        payload = decodeJWT(token)
        user_id = payload["user_id"].split("@")[0]

        result = client.fget_object(user_id, object_name, f"./datafolder/{object_name}")

        return f"./datafolder/{object_name}"



    @app.post("/upload_file", dependencies=[Depends(JWTBearer())], tags=["files"])
    def upload_file(file: UploadFile  = File(...), token: dict = Depends(JWTBearer())) -> dict:

        payload = decodeJWT(token)
        user_id = payload["user_id"].split("@")[0]

        result = client.fput_object(user_id, file.filename, file.file.fileno())

        return { 
            "object_name": result.object_name,
            "etag": result.etag,
            "version_id": result.version_id
        }

    return app



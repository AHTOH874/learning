import sqlite3

from fastapi import FastAPI, Body, Depends
from fastapi.middleware.cors import CORSMiddleware


from app.model import UserSchema, UserLoginSchema
from app.auth.auth_bearer import JWTBearer
from app.auth.auth_handler import signJWT


def create_app() -> FastAPI:
    
    con = sqlite3.connect('users.db')

    con.execute("create TABLE if not EXISTS users  (id INTEger PRIMARY KEY AUTOINCREMENT, email varchar, password varchar)")

    def check_user(data: UserLoginSchema):

        cursor = con.cursor()

        cursor.execute(f'SELECT EXISTS ( SELECT * FROM users WHERE email = "{data.email}" and password = "{data.password}")')

        result = cursor.fetchone()
        cursor.close()
        
        print(result[0])

        return result[0]


    def insert_user(data: UserLoginSchema):

        con.execute(f'INSERT INTO users (email, password) VALUES ("{data.email}", "{data.password}")')

    app = FastAPI()

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.get("/", tags=["root"]) 
    async def root_route() -> dict:
        return { "health": "ok"}

    @app.post("/user/signup", tags=["user"])
    async def create_user(user: UserSchema = Body(...)):
        insert_user(user)
        return signJWT(user.email)

    @app.post("/user/login", tags=["user"])
    async def user_login(user: UserLoginSchema = Body(...)):
        if check_user(user):
            return signJWT(user.email)
        return {
            "error": "Wrong login details!"
        }

    return app
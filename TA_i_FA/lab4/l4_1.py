import re

list_var = []
nums = r'^\s*[0-9]+\s*$'
list_word = ['-', '+', '*', '(', ')']

file = open('data4.txt', 'r')
file = file.read().splitlines()

while "" in set(file):
    file.remove("")

if not re.search(r'var\s+', file[0]):
    exit('Отсутствуетслово var')
if not re.search(r'^begin\s*$', file[1]):
    exit('Отсутствуетслово begin')
if not re.search(r'^end$', file[len(file) - 1]):
    exit('Отсутствует слово end либо имеется текст вне блока программы')

st = file[0]
if re.search(r'var\s+(.+)', st):
    if re.search(r'var\s+([a-zA-Z].*)', st):
        perems = re.search(r'var\s+([a-zA-Z][^:]*)', st).group(1)
        list_var = re.findall(r'[a-zA-Z]+', perems)
        for var in list_var:
            if len(var) > 10:
                exit('Длина переменной (' + var + ') не может быть больше 10')
        for s in range(len(list_var)):
            for s1 in range(len(list_var)):
                if s1 != s and list_var[s] == list_var[s1]:
                    exit('Повторное объявление переменной ' + list_var[s])
    else:
        exit('Недопустимое имя переменной')
else:
    exit('Отсутствуют имена переменных')

if not re.search(r':\s+integer\s*', st):
    exit('Отсутствует тип переменных')

if not re.search(r';$', st):
    exit('Отсутствует ; в конце строки')

body = file[2:-1]

def digORvar(st):
    if st in list_var or re.search(nums, st):
        return True
    return False

def splitExp(st):
    st = st.replace(" ","")
    arr = []
    while st != '':
        if st[0] in list_word+['/']:
            arr.append(st[0])
            st = st.replace(st[0],"",1)
        else:
            word = ''
            for s in st:
                if s not in list_word+['/']:
                    word += s
                else:
                    break
            arr.append(word)
            st = st.replace(word,"",1)
    return arr

def assign(st):
    if re.search(r'^([^ =]+)', st):
        var = re.search(r'^([^ =]+)', st).group().strip()
        if var not in list_var:
            exit('Неизвестная переменная ' + var + ' на строке ' + st)
    else:
        exit('Отсутствует имя переменной: ' + st)

    if not re.search(r'=', st):
        exit('Отсутствует присвоение: ' + st)
    if not re.search(r'=\s*([^; ]+)', st):
        exit('Отсутствует выражение: ' + st)
    if not re.search(r';$', st):
        exit('Отсутствует ; в конце строки: ' + st)

def expression(st):
    exp = re.search(r'=(.*);', st).group(1)
    arr = splitExp(exp)

    n = len(arr)
    o_brackets = exp.count('(')
    c_brackets = exp.count(')')

    if o_brackets < c_brackets:
        exit('Отсутствует открывающая скобка: ' + exp)
    if c_brackets < o_brackets:
        exit('Отсутствует закрывающая скобка: ' + exp)

    for word in arr:
        if word not in list_var + list_word and not re.search(nums, word):
            exit('Неизвестная переменная ' + word + ' на строке ' + st)

        for i in range(n):
            if digORvar(arr[i]):
                if i != n - 1 and digORvar(arr[i + 1]):
                    exit('После переменной должен быть операнд: ' + st)
                if arr[i] == '+' or arr[i] == '*':
                    if i == 0 or not (digORvar(arr[i - 1]) or arr[i-1] in ['(', ')']):
                        exit('Перед знаком '+arr[i]+' должна стоять переменная: ' + st)
                    if i == n - 1 or not (digORvar(arr[i + 1]) or arr[i+1] in ['(', ')']):
                        exit('После знака '+arr[i]+' должна стоять переменная: ' + st)
                if arr[i] == '-':
                        if i == n - 1 or not (digORvar(arr[i + 1]) or arr[i+1] in ['(', ')']):
                            exit('После знака - должна стоять переменная: ' + st)


for line in body:
    assign(line)
    expression(line)

print('Программа работает корректно')

import math

class Point:
    """
    Базовый класс для точек
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f'x: {self.x}, y: {self.y}'


class CartesianPoint(Point):
    """
    Точка в декартовой системе координат
    """

    def __init__(self, x, y):
        super().__init__(x, y)


class PolarPoint(Point):
    """
    Точка в полярной системе координат
    """

    def __init__(self, polar_radius, polar_angle):
        x = polar_radius * math.cos(polar_angle)
        y = polar_radius * math.sin(polar_angle)
        super().__init__(x, y)


class PointCreator():

    def create_point(self, c_system, x, y) -> Point:
        """
        Factory Method
        """
        factory_dict = {
            "Cartesian": CartesianPoint,
            "Polar": PolarPoint,
        }
        return factory_dict[c_system](x, y)


if __name__ == '__main__':
    p_factory = PointCreator()
    cartesian_point = p_factory.create_point("Cartesian", 5, 6)
    polar_point = p_factory.create_point("Polar", 4, 6)
    print(cartesian_point)
    print(polar_point)
class AbstractHandler(object):
    """Абстарктный класс для всех хендлеров"""

    def __init__(self, nxt):
        """Указатель на следующий хендлер"""

        self._nxt = nxt

    def handle(self, request):
        """Вызов обработки запроса"""

        handled = self.processRequest(request)

        """Если запрос не обработан, он обрабатывается 
        другим хендлером"""

        if not handled:
            self._nxt.handle(request)

    def processRequest(self, request):
        """Абстрактный метод обработки запроса"""

        raise NotImplementedError()


class FirstConcreteHandler(AbstractHandler):
    """Обрабатывает символы от 'a' до 'e' """

    def processRequest(self, request):
        '''Возвращает True, если запрос обработан'''

        if 'a' < request <= 'e':
            print("Хендлер {} обработал запрос '{}'".format(self.__class__.__name__, request))
            return True


class SecondConcreteHandler(AbstractHandler):
    """Обрабатывает символы от 'e' до 'l' """

    def processRequest(self, request):
        '''Возвращает True, если запрос обработан'''

        if 'e' < request <= 'l':
            print("Хендлер {} обработал запрос '{}'".format(self.__class__.__name__, request))
            return True


class ThirdConcreteHandler(AbstractHandler):
    """Обрабатывает символы от 'l' до 'z' """

    def processRequest(self, request):
        '''Возвращает True, если запрос обработан'''

        if 'l' < request <= 'z':
            print("Хендлер {} обработал запрос '{}'".format(self.__class__.__name__, request))
            return True


class DefaultHandler(AbstractHandler):
    """Обрабатывает все символы, которые не удалось обработать конкретными хендлерами"""

    def processRequest(self, request):
        """Выдает сообщение о том, что запрос не обработан и возвращает true"""

        print("{} сообщает вам, что у запроса '{}' нет обработчика".format(self.__class__.__name__,
                                                                                          request))
        return True


class User:
    """Класс пользователя"""

    def __init__(self):
        """Инициализирует цепочку обработчиков для пользователя"""

        initial = None

        self.handler = FirstConcreteHandler(
            SecondConcreteHandler(
                ThirdConcreteHandler(
                    DefaultHandler(initial))))

    def agent(self, user_request):
        """Перебирает каждый запрос и отправляет их в цепочку обработчиков"""

        for request in user_request:
            self.handler.handle(request)


if __name__ == "__main__":
    user = User()
    string = "TestString"
    requests = list(string)
    user.agent(requests)
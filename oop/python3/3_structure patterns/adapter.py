import abc
from abc import ABCMeta


class IScale(metaclass=ABCMeta):

    """Абстрактные весы"""

    @abc.abstractmethod
    def get_weight(self):
        pass


class RussianScales(IScale):

    """Российские весы (ед. измерения - кг)"""

    def __init__(self, cw):
        self.__current_weight = cw

    def get_weight(self):
        return self.__current_weight


class BritishScales(IScale):

    """Британские весы (ед. измерения - фунты)"""

    def __init__(self, cw):
        self.__current_weight = cw

    def get_weight(self):
        return self.__current_weight


class AdapterForBritishScales(IScale):

    """Адаптер для работы британских весов с
    российскими единицами измерения"""

    def __init__(self, british_scales):
        self.__british_scales = british_scales

    def get_weight(self):
        return self.__british_scales.get_weight() * .453


if __name__ == "__main__":
    obj_in_kg = 55 # В килограмммах
    obj_in_lb = 55 # В фунтах

    rScales = RussianScales(obj_in_kg)
    bScales = BritishScales(obj_in_lb)
    baScales = AdapterForBritishScales(bScales)

    print(f"Российские весы - {rScales.get_weight()} килограмм")
    print(f"Британские весы - {bScales.get_weight()} фунтов")
    print(f"Результат - {baScales.get_weight()} килограмм")